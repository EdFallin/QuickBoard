﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace QuickBoard
{
    /* an individual thing visible in the QuickBoard window, just text, for now at least */

    [Serializable]
    public class Item : INotifyPropertyChanged
    {
        #region Definitions

        // no leading ×@ on these, which turns escaped chars into literals 
        public const string RETURN_NEW = "\r\n";
        public const string NEW = "\n";

        public static readonly string[] SPLITTERS = { RETURN_NEW, NEW };
        public const int MAX_HOVER_LINES = 25;

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Definitions


        #region Static properties

        #region Definitions

        public static Item Empty  /* verified */  {
            get {
                return (new Item());
            }

            /* no setter */
        }

        public static string[] Splitters  /* verified */  { get => SPLITTERS; }

        #endregion Definitions


        #region Clipboard

        /* although these can change, they do not support INotifyPropertyChanged; 
         * used only by RoutedUICommand event handlers that call them whenever needed */

        public static bool CanInsertFromClipboard  /* passed */  {
            get {
                try {
                    bool canPaste = Clipboard.ContainsText()
                        || Clipboard.ContainsData(ClipboardFormats.QuickBoardItem);
                    return (canPaste);
                }
                catch {
                    return (false);
                }
            }
        }

        public static bool CanInsertTextFromClipboard  /* passed */  {
            get {
                try {
                    bool canPaste = Clipboard.ContainsText();
                    return (canPaste);
                }
                catch {
                    return (false);
                }
            }
        }

        #endregion Clipboard

        #endregion Static properties


        #region Fields

        string _content;
        bool _isHeader;
        bool _wasCopied;
        bool _isGrouped;

        #endregion Fields


        #region Properties

        /// <summary>
        /// Returns the unaltered text content of the Item. 
        /// When set, fires any PropertyChanged event for .Content, 
        /// and fires any PropertyChanged for .IsEmpty if any nulls. 
        /// </summary>
        public string Content  /* passed & ok */  {
            get  /* ok */  {
                return (_content);
            }
            set  /* passed */  {
                bool eitherIsNull = (_content == null || value == null);

                _content = value;

                this.OnPropertyChanged(nameof(this.Content));

                // changes to .Content change .DisplayableContent 
                this.DisplayableContent = _content;
                this.OnPropertyChanged(nameof(this.DisplayableContent));

                // changes to .Content can change .IsEmpty 
                if (eitherIsNull) {
                    this.OnPropertyChanged(nameof(this.IsEmpty));
                }
            }
        }

        [XmlIgnore]
        public string DisplayableContent  /* verified */  {
            get {
                // encountered in some edge cases, for instance some steps after clearing a Board 
                if (this.IsEmpty) {
                    return (this.Content);  // null 
                }

                string[] asLines = this.Content.Split(SPLITTERS, StringSplitOptions.None);  // limiting number here doesn't discard excess; instead, it's all in last string 
                int maxLines = Math.Min(asLines.Length, MAX_HOVER_LINES);
                string displayable = string.Join(Environment.NewLine, asLines, 0, maxLines);

                return (displayable);
            }
            protected set {
                /* nothing set here, because value is always (re)calculated from .Content */

                this.OnPropertyChanged(nameof(this.DisplayableContent));
            }
        }

        public bool IsHeader  /* verified & ok */  {
            get  /* ok */  { return (_isHeader); }
            set  /* verified */  {
                _isHeader = value;
                this.OnPropertyChanged(nameof(this.IsHeader));
            }
        }

        /// <summary>
        /// Returns true if .Content is null.
        /// Returns false if .Content is non-null, even if all whitespace. 
        /// </summary>
        [XmlIgnore]
        public bool IsEmpty  /* passed */  {
            get {
                // whitespace does not count as empty 
                return (_content == null);
            }
            /* no setter */
        }

        /// <summary>
        /// Returns true if .Content contains any strings in .Splitters / SPLITTERS.
        /// Returns false if .Content doesn't contain any of those, or is null.
        /// </summary>
        [XmlIgnore]
        public bool CanSplit  /* passed */  {
            get {
                // just in case 
                if (_content == null) {
                    return (false);
                }

                // main path: can only be split if any splitter present 
                for (int i = 0; i < SPLITTERS.Length; i++) {
                    if (_content.Contains(SPLITTERS[i])) {
                        return (true);
                    }
                }

                return (false);
            }
        }

        [XmlIgnore]
        public bool WasCopied  /* passed & ok */  {
            get  /* ok */  {
                return (_wasCopied);
            }
            set  /* passed */  {
                bool isChanging = (_wasCopied != value);
                _wasCopied = value;

                    this.OnPropertyChanged(nameof(this.WasCopied));
            }
        }

        [XmlIgnore]
        public bool IsGrouped  /* verified */  {
            get {
                return (_isGrouped);
            }
            set {
                _isGrouped = value;
                this.OnPropertyChanged(nameof(this.IsGrouped));
            }
        }

        #endregion Properties


        #region Constructors

        public Item(string content, bool isHeader) : this(content)  /* verified */ {
            /* skipping property and :. notification at init */
            _isHeader = isHeader;
        }

        public Item(string content) : this()  /* verified */  {
            /* skipping property and :. notification at init */
            _content = content;
        }

        /* any operations that should always be performed belong here */
        public Item()  /* ok */  {
            /* no operations */
        }

        #endregion Constructors


        #region Methods

        public EditExitType Edit(EditType editType)  /* verified */  {
            EditItemWindow editor = new EditItemWindow(this, editType);

            editor.ShowDialog();  // built-in result type ignored 
            EditExitType exitType = editor.ExitType;

            return (exitType);
        }

        public void ToClipboard()  /* passed */  {
            if (_content == null) {
                return;
            }

            try {
                string serialized = this.Serialize();

                DataObject toClipboard = new DataObject();
                toClipboard.SetText(_content);
                toClipboard.SetData(ClipboardFormats.QuickBoardItem, serialized);

                Clipboard.SetDataObject(toClipboard, true);
                this.WasCopied = true;
            }
            catch (Exception x) {
                MessageBox.Show(x.ToString());
            }
        }

        /* dependency of ToClipboard() */
        public string Serialize()  /* passed */  {
            XmlSerializer serializer = new XmlSerializer(typeof(Item));
            StringBuilder asBuilder = new StringBuilder();

            using (StringWriter writer = new StringWriter(asBuilder)) {
                serializer.Serialize(writer, this);
            }

            string asString = asBuilder.ToString();
            return (asString);
        }

        public void ToClipboardStart()  /* passed */  {
            if (_content == null) {
                return;
            }

            try {
                string existing = Clipboard.GetText();
                string combined = this.CombineWithLineBreak(this.Content, existing);

                Clipboard.SetText(combined);
            }
            catch (Exception x) {
                MessageBox.Show(x.ToString());
            }
        }

        public void ToClipboardEnd()  /* passed */  {
            if (_content == null) {
                return;
            }

            try {
                string existing = Clipboard.GetText();
                string combined = this.CombineWithLineBreak(existing, this.Content);

                Clipboard.SetText(combined);
            }
            catch (Exception x) {
                MessageBox.Show(x.ToString());
            }
        }

        public List<Item> Split()  /* passed */  {
            // splitting the underlying content 
            string[] asSplit = this.Content.Split(SPLITTERS, StringSplitOptions.RemoveEmptyEntries);
            List<Item> asItems = new List<Item>();

            // replacing contents of current item, always first, 
            // and including in output 
            this.Content = asSplit[0];
            asItems.Add(this);

            // any text split off converted to Items, added to output in order; 
            // all split-off Items have same .IsHeader as original (@this) 
            for (int i = 1; i < asSplit.Length; i++) {
                Item fromIth = new Item(asSplit[i]);
                fromIth.IsHeader = this.IsHeader;
                asItems.Add(fromIth);
            }

            // output 
            return (asItems);
        }

        public void AddFromClipboardAtStart()  /* passed */  {
            try {
                string clipboardText = Clipboard.GetText();

                if (!string.IsNullOrEmpty(clipboardText)) {
                    this.AddAtStart(clipboardText);
                }
            }
            catch (Exception x) {
                MessageBox.Show(x.ToString());
            }
        }

        public void AddAtStart(string content)  /* passed */  {
            this.Content = this.CombineWithLineBreak(content, this.Content);
        }

        public void AddAtStart(List<Item> items)  /* passed */  {
            List<string> contents = items
                .Select(x => x.Content)
                .ToList();

            // all contents are stripped of any trailing line break for simpler joining 
            for (int i = 0; i < contents.Count; i++) {
                contents[i] = Item.RemoveTrailingLineBreak(contents[i]);
            }

            // simplifying for joining 
            contents.Add(this.Content);

            // adding 
            this.Content = string.Join(RETURN_NEW, contents);
        }

        public void AddFromClipboardAtEnd()  /* passed */  {
            try {
                string clipboardText = Clipboard.GetText();

                if (!string.IsNullOrEmpty(clipboardText)) {
                    this.AddAtEnd(clipboardText);
                }
            }
            catch (Exception x) {
                MessageBox.Show(x.ToString());
            }
        }

        public void AddAtEnd(string content)  /* passed */  {
            string passer = Item.RemoveTrailingLineBreak(this.Content);
            this.Content = this.CombineWithLineBreak(this.Content, content);
        }

        public void AddAtEnd(List<Item> items)  /* passed */  {
            List<string> contents = items
                .Select(x => x.Content)
                .ToList();

            // simplifying for trimming and joining 
            contents.Insert(0, this.Content);

            // all contents except last are stripped of any trailing line break for simpler joining 
            for (int i = 0; i < contents.Count - 1; i++) {
                contents[i] = Item.RemoveTrailingLineBreak(contents[i]);
            }

            // adding 
            this.Content = string.Join(RETURN_NEW, contents);
        }

        /* dependency of several methods */
        private string CombineWithLineBreak(string first, string second)  /* passed */  {
            first = Item.RemoveTrailingLineBreak(first);
            string combined = first + RETURN_NEW + second;

            return (combined);
        }

        #endregion Methods


        #region Static methods

        public static Item FromClipboard()  /* passed */  {
            Item item = null;

            try {
                // if an Item is on clipboard, deserialize and return that 
                string clipboardSerial = Clipboard.GetData(ClipboardFormats.QuickBoardItem) as string;
                item = Item.FromSerial(clipboardSerial);

                if (item != null) {
                    return item;
                }

                // if text on clipboard but not Item, make new Item from text and return that 
                string clipboardText = Clipboard.GetText();

                if (!string.IsNullOrEmpty(clipboardText)) {
                    item = new Item(clipboardText);
                    return item;
                }
            }
            catch (Exception x) {
                MessageBox.Show(x.ToString());
            }

            // when no Item or text on clipboard, or an Exception thrown 
            return (item);
        }

        public static Item FromClipboardText()  /* passed */  {
            Item item = null;

            try {
                // if text on clipboard, make new Item from it and return that 
                string clipboardText = Clipboard.GetText();

                if (!string.IsNullOrEmpty(clipboardText)) {
                    item = new Item(clipboardText);
                    return item;
                }
            }
            catch (Exception x) {
                MessageBox.Show(x.ToString());
            }

            // when no text on clipboard, or an Exception thrown 
            return (item);
        }

        /* dependency of FromClipboard() */
        public static Item FromSerial(string serial)  /* passed */  {
            Item item = null;

            if (string.IsNullOrWhiteSpace(serial)) {
                return (item);
            }

            using (StringReader reader = new StringReader(serial)) {
                using (XmlTextReader xml = new XmlTextReader(reader)) {
                    xml.WhitespaceHandling = WhitespaceHandling.All;

                    XmlSerializer serializer = new XmlSerializer(typeof(Item));

                    // CanDeserialize() throws an Exception if @serial is not XML at all; 
                    // if not XML or if invalid Item XML, returns anti-default for consumers 
                    try {
                        if (!serializer.CanDeserialize(xml)) {
                            return (item);
                        }

                    }
                    catch (Exception) {
                        return (item);
                    }

                    item = (Item)serializer.Deserialize(xml);
                }
            }

            return (item);
        }

        public static Item FromItems(List<Item> items)  /* passed */  {
            string[] contents = items
                .Select(x => x.Content)
                .ToArray();

            // all contents except last are stripped of any trailing line break for simpler joining 
            for (int i = 0; i < contents.Length - 1; i++) {
                contents[i] = Item.RemoveTrailingLineBreak(contents[i]);
            }

            string content = string.Join(RETURN_NEW, contents);

            Item merged = new Item(content, items[0].IsHeader);  // retaining any header state 
            return (merged);
        }

        /* dependency of several methods */
        private static string RemoveTrailingLineBreak(string text)  /* passed */  {
            // either \r\n or \n is removed if at end 

            int removeFrom = 0;

            if (text.EndsWith(RETURN_NEW)) {
                removeFrom = text.LastIndexOf(RETURN_NEW);
            }
            else if (text.EndsWith(NEW)) {
                removeFrom = text.LastIndexOf(NEW);
            }

            if (removeFrom > 0) {
                text = text.Substring(0, removeFrom);
            }

            return (text);
        }

        #endregion Static methods


        #region INotifyPropertyChanged

        protected void OnPropertyChanged(string propertyName)  /* verified */  {
            PropertyChangedEventArgs args = new PropertyChangedEventArgs(propertyName);
            this.PropertyChanged?.Invoke(this, args);
        }

        #endregion INotifyPropertyChanged

    }
}
