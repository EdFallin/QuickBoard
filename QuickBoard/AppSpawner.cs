﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Diagnostics;

namespace QuickBoard
{
    /* encapsulates processes that create new QuickBoard instances */
    public class AppSpawner
    {
        const string SAFE_ARG = @""".*""";  // with beginning and ending double quotes 

        public static void SpawnFromBoard(Board board)  /* verified */  {
            // exit without spawning a new app instance 
            if (!board.CanSave) {
                return;
            }

            // opening the new board right away in a new QuickBoard instance 
            string[] appArgs = Environment.GetCommandLineArgs();
            string appPath = appArgs[0];

            string boardPath = AppSpawner.AsSafeArg(board.SiteOnDisk);

            Process.Start(appPath, boardPath);
        }

        /// <summary>
        /// Adds quotes around @path, if not present, so it can be used as a command-line arg.
        /// Quotes are only needed with paths containing spaces, 
        /// but there is no reason not to use them all the time.
        /// </summary>
        private static string AsSafeArg(string path)  /* passed */  {
            // no changes needed 
            if (Regex.IsMatch(path, SAFE_ARG)) {
                return (path);
            }

            // quotes needed; it's assumed that quotes are never present only at one end 
            path = $"\"{ path }\"";
            return (path);
        }
    }
}
