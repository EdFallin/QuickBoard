﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Win32;
using System.Windows;

namespace QuickBoard
{
    /* a source for a collection of items displayed at the same time in the QuickBoard window; 
     * implemented against an XML file */

    [Serializable]
    public class Board : ObservableCollection<Item>
    {
        /* Board encapsulates an entire collection of Items to be worked with together; 
         * the private member .Grouped supports operations on Items selected; 
         * most or all operations on groups of Items should be handled by adding to .Grouped 
         * with methods for that, then using methods that act on .Grouped */

        /* an ObservableCollection<T> is also an INotifyPropertyChanged, but generally don't call ×OnPropertyChanged() */

        #region Definitions

        const string FILE_EXT = @".board";  // must include leading @. 

        const string DIALOG_FILE_FILTERS = @"Board Files (*.board)|*.board|All Files (*.*)|*.*";

        #endregion Definitions


        #region Fields

        string _siteOnDisk;
        string _siteName;

        List<Item> _grouped = new List<Item>();

        #endregion Fields


        #region Static properties

        public static bool CanInsertItemsFromClipboard  /* passed */  {
            get {
                try {
                    bool canPaste = Clipboard.ContainsData(ClipboardFormats.QuickBoardItemArray);
                    return (canPaste);
                }
                catch {
                    return (false);
                }
            }
        }

        #endregion Static properties


        #region Properties

        /// <summary>
        /// The path and file name for this Board.
        /// </summary>
        public string SiteOnDisk  /* ok */  {
            get {
                return (_siteOnDisk);
            }
            set {
                /* although ObservableCollection<T> is INotifyPropertyChanged, with an OnPropertyChanged() already defined, 
                 * changes to properties should be observed differently or simply not have explicit observation, 
                 * since having explicit observation by calling OnPropertyChanged() on a setter causes fails */
                _siteOnDisk = value;
                this.SiteName = this.SiteToName(_siteOnDisk);
            }
        }

        /// <summary>
        /// The readable file name for this Board.
        /// </summary>
        public string SiteName  /* ok */  {
            get {
                return (_siteName);
            }
            protected set {
                _siteName = value;
                this.OnPropertyChanged(new PropertyChangedEventArgs(nameof(SiteName)));
            }
        }

        public bool CanSave  /* passed */  {
            get {
                // an empty board can be saved, but not one with no related file 

                bool hasFile = (!string.IsNullOrEmpty(_siteOnDisk));
                return (hasFile);
            }

            /* no setter */
        }

        protected List<Item> Grouped  /* ok */  {
            get {
                return (_grouped);
            }

            /* no setter: elements of List can be set, but not whole List  */
        }

        public int CountOfGrouped  /* verified */  {
            get => _grouped.Count;
        }

        #endregion Properties


        #region Constructors

        public Board()  /* verified */  : base() {
            /* no operations; needed for deserialization */
        }

        public Board(List<Item> items)  /* verified */  : base(items) {
            /* no operations */
        }

        #endregion Constructors


        #region Public methods and dependencies

        #region Saving Board as file

        public void Save()  /* verified */  {
            XmlSerializer serializer = new XmlSerializer(typeof(Board));

            using (Stream stream = File.Create(this.SiteOnDisk)) {
                serializer.Serialize(stream, this);
                stream.Flush();
            }
        }

        public void SaveAs()  /* verified */  {
            SaveFileDialog dialog = this.SupplySaveAsDialog();
            bool? result = dialog.ShowDialog(Application.Current.MainWindow);  // arg makes dialog open near app's window 
            bool didClickOK = result ?? false;

            if (!didClickOK) {
                return;
            }

            // user can't exit with true unless they choose a file name; 
            // the dialog has already validated path and so on 
            this.SiteOnDisk = dialog.FileName;
            this.Save();
        }

        private SaveFileDialog SupplySaveAsDialog()  /* verified */  {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.AddExtension = true;
            dialog.CheckFileExists = false;
            dialog.CheckPathExists = true;
            dialog.CreatePrompt = false;
            dialog.DefaultExt = FILE_EXT;
            dialog.DereferenceLinks = true;
            dialog.Filter = DIALOG_FILE_FILTERS;
            dialog.FilterIndex = 0;
            dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            dialog.OverwritePrompt = true;
            dialog.Title = "Save As";
            dialog.ValidateNames = true;

            return (dialog);
        }

        #endregion Saving Board as file


        #region Working from one Item to several

        public void SplitItem(Item item)  /* passed */  {
            // the actual splitting 
            List<Item> asItems = item.Split();

            // replacing the original Item 
            int at = this.IndexOf(item);
            this[at] = asItems[0];

            // dropping first Item and setting insert point to after it 
            asItems.RemoveAt(0);
            at++;

            // inserting the remaining Items 
            this.InsertItems(asItems, at);
        }

        #endregion Working from one Item to several


        #region Grouping Items

        public void AddToGrouped(Item item)  /* passed */  {
            // an Item is never added twice, for reliability and predictability 
            if (this.Grouped.Contains(item)) {
                return;
            }

            this.Grouped.Add(item);
            item.IsGrouped = true;
        }

        public void AddToGrouped(params Item[] items)  /* passed */  {
            /* calls AddToGrouped( Item ) for consistency */

            foreach (Item item in items) {
                this.AddToGrouped(item);
            }
        }

        public void RemoveFromGrouped(Item item)  /* passed */  {
            this.Grouped.Remove(item);
            item.IsGrouped = false;
        }

        public void RemoveFromGrouped(params Item[] items)  /* passed */  {
            /* calls RemoveFromGrouped( Item ) for consistency */

            foreach (Item item in items) {
                this.RemoveFromGrouped(item);
            }
        }

        public void ClearGrouped()  /* passed */  {
            // clearing flag on all Items, not just those in .Grouped, just in case 
            foreach (Item item in this) {
                item.IsGrouped = false;
            }

            this.Grouped.Clear();
        }

        #endregion Grouping Items


        #region Working with .Grouped

        #region Retrieving Items

        public Item GroupedItem(int index)  /* passed */  {
            if (index < 0 || index >= _grouped.Count) {
                return (null);
            }

            return (_grouped[index]);
        }

        #endregion Retrieving Items


        #region Other non-clipboard operations

        public void MergeGrouped()  /* passed */  {
            // the actual merging 
            Item merged = Item.FromItems(this.Grouped);

            // replacing first Item with new, merged Item 
            int index = this.IndexOf(this.Grouped[0]);
            this[index] = merged;

            // removing all other Items; @merged is not in .Grouped, so not removed 
            this.RemoveGrouped();
        }

        public void ToggleHeaderOnGrouped()  /* passed */  {
            // all end up with the same header state, opposite that of the first Item when it arrives 
            bool setHeader = !(this.Grouped[0].IsHeader);

            foreach (Item item in this.Grouped) {
                item.IsHeader = setHeader;
            }
        }

        public void ClearWasCopiedOnGrouped()  /* passed */  {
            // flag is cleared whether previously set or not 
            foreach (Item item in this.Grouped) {
                item.WasCopied = false;
            }
        }

        public Board GroupedToNewBoard()  /* verified */  {
            Board newBoard = new Board(this.Grouped);
            newBoard.SaveAs();

            // if not saved, exiting early so grouped Items are not lost 
            if (!newBoard.CanSave) {
                return (null);
            }

            // only clear .Grouped / Item.IsGrouped if new Board actually created 
            this.RemoveGrouped();
            return (newBoard);
        }

        public void RemoveGrouped()  /* passed */  {
            while (this.CountOfGrouped > 0) {
                Item item = this.Grouped.First();
                this.Remove(item);
            }
        }

        #endregion Other non-clipboard operations


        #region Copying to clipboard

        public void CopyGroupedToClipboard()  /* passed */  {
            /* when copying as whole clipboard contents, combined .Contents of .Grouped 
             * are saved as text, and an Item[] of .Grouped are saved as custom data */
            Board.ItemsToClipboard(this.Grouped);
        }

        public void CopyGroupedToClipboardStart()  /* passed */  {
            /* Items are collected and then pasted once by collected Item; 
             * since added to previous text, not also kept as Item[] */

            Item combined = Item.FromItems(this.Grouped);
            combined.ToClipboardStart();
        }

        public void CopyGroupedToClipboardEnd()  /* passed */  {
            /* Items are collected and then pasted once by collected Item; 
             * since added to previous text, not also kept as Item[] */

            Item combined = Item.FromItems(this.Grouped);
            combined.ToClipboardEnd();
        }

        #endregion Copying to clipboard


        #region Cutting to clipboard

        public void CutGroupedToClipboard()  /* passed */  {
            this.CopyGroupedToClipboard();
            this.RemoveGrouped();
        }

        public void CutGroupedToClipboardStart()  /* passed */  {
            this.CopyGroupedToClipboardStart();
            this.RemoveGrouped();
        }

        public void CutGroupedToClipboardEnd()  /* passed */  {
            this.CopyGroupedToClipboardEnd();
            this.RemoveGrouped();
        }

        #endregion Cutting to clipboard

        #endregion Working with .Grouped


        #region Inserting sets of Items

        public void InsertItemsFromClipboard(int at)  /* passed */  {
            try {
                if (!Clipboard.ContainsData(ClipboardFormats.QuickBoardItemArray)) {
                    return;
                }
            }
            catch (Exception x) {
                MessageBox.Show(x.ToString());
                return;
            }

            List<Item> items = Board.ItemsFromClipboard();
            this.InsertItems(items, at);
        }

        public void InsertItems(List<Item> items, int at)  /* passed */  {
            for (int from = 0, to = at; from < items.Count; from++, to++) {
                this.InsertItem(to, items[from]);
            }
        }

        #endregion Inserting sets of Items


        #region Working with all Items

        public void ClearAllWasCopiedFlags()  /* passed */  {
            foreach (Item item in this) {
                item.WasCopied = false;
            }
        }

        public void CopyAllItemsToClipboard()  /* passed */  {
            List<Item> allItems = this.ToList();
            Board.ItemsToClipboard(allItems);
        }

        #endregion Working with all Items

        #endregion Public methods and dependencies


        #region Other private methods

        /* a dependency of one of .SiteOnDisk and, indirectly, of .SiteName */

        /// <summary>
        /// Converts a path to a readable file name.
        /// </summary>
        private string SiteToName(string site)  /* passed */  {
            string name = site ?? string.Empty;
            name = Path.GetFileNameWithoutExtension(name);

            return (name);
        }

        #endregion Other private methods


        #region Static methods

        #region On whole Board

        public static Board Load()  /* verified */  {
            List<string> args = Environment.GetCommandLineArgs().ToList();
            args.RemoveAt(0);  // first arg is the app itself 

            string arg = null;

            if (args.Count > 0) {
                if (File.Exists(args[0])) {
                    arg = args[0];
                }
            }

            return (Load(arg));
        }

        public static Board Load(string siteOnDisk)  /* verified */  {
            // if no file, open the app with an empty board 
            if (string.IsNullOrWhiteSpace(siteOnDisk)) {
                return (new Board());
            }

            // if not a board file, warn user and open an empty board 
            if (!siteOnDisk.EndsWith(FILE_EXT)) {
                MessageBox.Show(
                    "The chosen file is not a board.\r\n\r\nAn empty board is being opened instead.",
                    "Non-Board File Type");
                return (new Board());
            }

            // deserialize the board file if possible 
            XmlSerializer deserializer = new XmlSerializer(typeof(Board));
            Board board = null;

            // using an XmlTextReader to preserve any all- white-space .Contents of Items 
            using (Stream stream = File.OpenRead(siteOnDisk)) {
                using (XmlTextReader reader = new XmlTextReader(stream)) {
                    reader.WhitespaceHandling = WhitespaceHandling.All;

                    // actually loading the board 
                    board = (Board)deserializer.Deserialize(reader);

                    reader.Close();
                }

                stream.Close();
            }

            // so the Board can save itself 
            board.SiteOnDisk = siteOnDisk;

            return (board);
        }

        #endregion On whole Board


        #region Clipboard-related

        public static void ItemsToClipboard(List<Item> items)  /* passed */  {
            /* .Contents of Items in .Grouped are combined and saved as text, and alongside, 
             * Items in .Grouped are saved as custom data as a serialized Item[] */

            if (items.Count == 0) {
                return;
            }

            try {
                // the two kinds of data to set on clipboard 
                Item combined = Item.FromItems(items);
                string content = combined.Content;
                string serialized = Board.SerializeItems(items);

                // placing both in a DataObject 
                DataObject toClipboard = new DataObject();
                toClipboard.SetText(content);
                toClipboard.SetData(ClipboardFormats.QuickBoardItemArray, serialized);

                // setting clipboard to DataObject 
                Clipboard.SetDataObject(toClipboard, true);
            }
            catch (Exception x) {
                MessageBox.Show(x.ToString());
            }
        }

        public static string SerializeItems(List<Item> items)  /* passed */  {
            XmlSerializer serializer = new XmlSerializer(typeof(Item[]));
            StringBuilder asBuilder = new StringBuilder();

            using (StringWriter writer = new StringWriter(asBuilder)) {
                Item[] asArray = items.ToArray();
                serializer.Serialize(writer, asArray);
            }

            string asString = asBuilder.ToString();
            return (asString);
        }

        public static List<Item> ItemsFromClipboard()  /* passed */  {
            List<Item> items = new List<Item>();

            try {
                string clipboardSerial = (string)Clipboard.GetData(ClipboardFormats.QuickBoardItemArray);
                items = Board.ItemsFromSerial(clipboardSerial);
                return (items);
            }
            catch (Exception x) {
                MessageBox.Show(x.ToString());
            }

            // if an Exception, empty List returned 
            return (items);
        }

        public static List<Item> ItemsFromSerial(string serial)  /* passed */  {
            List<Item> items = new List<Item>();

            if (string.IsNullOrWhiteSpace(serial)) {
                return (items);
            }

            using (StringReader reader = new StringReader(serial)) {
                using (XmlTextReader xml = new XmlTextReader(reader)) {
                    xml.WhitespaceHandling = WhitespaceHandling.All;

                    // data stored externally as an array of Items 
                    XmlSerializer serializer = new XmlSerializer(typeof(Item[]));

                    // ensuring that @serial can be deserialized; 
                    // CanDeserialize() throws Exception if not XML 
                    try {
                        if (!serializer.CanDeserialize(xml)) {
                            return (items);
                        }
                    }
                    catch (Exception) {
                        return (items);
                    }

                    Item[] asArray = (Item[])serializer.Deserialize(xml);
                    items = asArray.ToList();
                }
            }

            return (items);
        }

        #endregion Clipboard-related

        #endregion Static methods


        #region Overrides

        /// <summary>
        /// Overrides base.Remove() so Board can also drop @item from .Grouped.
        /// The base method is not ×virtual, so this is declared with @new, not ×override.
        /// </summary>
        public new bool Remove(Item item)  /* passed */  {
            this.Grouped.Remove(item);
            return (base.Remove(item));
        }

        #endregion Overrides
    }
}
