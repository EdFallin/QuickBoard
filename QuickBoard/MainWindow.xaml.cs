﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace QuickBoard
{
    public partial class MainWindow : Window
    {
        #region Definitions

        const double SPACE_BELOW = 5d;  // space below the window when content won't fit on screen 

        #endregion Definitions


        #region Fields

        Board _board;

        // used when adding a new Item 
        Item _cachedItem;
        int _cachedOffset = -1;

        #endregion Fields


        #region Static Command properties

        public static Command ToggleAlwaysOnTop { get; protected set; }
        public static Command Save { get; protected set; }
        public static Command SaveAs { get; protected set; }
        public static Command CloseSelf { get; protected set; }

        public static Command CopyItem { get; protected set; }
        public static Command CutItem { get; protected set; }
        public static Command EditItem { get; protected set; }
        public static Command DeleteItem { get; protected set; }
        public static Command SplitItem { get; protected set; }
        public static Command ToggleGrouped { get; protected set; }
        public static Command ToggleStretchGrouped { get; protected set; }
        public static Command ToggleHeader { get; protected set; }
        public static Command ForgetItemHistory { get; protected set; }

        public static Command CopyItemToStart { get; protected set; }
        public static Command CopyItemToEnd { get; protected set; }
        public static Command CutItemToStart { get; protected set; }
        public static Command CutItemToEnd { get; protected set; }
        public static Command AddAtItemStart { get; protected set; }
        public static Command AddAtItemEnd { get; protected set; }

        public static Command InsertFromClipboard { get; protected set; }
        public static Command InsertTextFromClipboard { get; protected set; }
        public static Command AddNewItem { get; protected set; }

        public static Command MergeSelections { get; protected set; }
        public static Command UnselectSelections { get; protected set; }
        public static Command ForgetHistoryOfSelections { get; protected set; }
        public static Command CopySelections { get; protected set; }
        public static Command CutSelections { get; protected set; }
        public static Command CopySelectionsToStart { get; protected set; }
        public static Command CopySelectionsToEnd { get; protected set; }
        public static Command CutSelectionsToStart { get; protected set; }
        public static Command CutSelectionsToEnd { get; protected set; }
        public static Command ToggleHeaderOnSelections { get; protected set; }
        public static Command MoveSelectionsToNewBoard { get; protected set; }
        public static Command DeleteSelections { get; protected set; }

        public static Command SelectAllItems { get; protected set; }
        public static Command CopyAllItems { get; protected set; }
        public static Command ForgetBoardHistory { get; protected set; }
        public static Command DeleteAllItems { get; protected set; }

        #endregion Static Command properties


        #region Constructors

        public MainWindow()  /* verified */  {
            // opens any Board provided as arg as program opens, otherwise an empty Board 
            this.DataContext = Board.Load();
            _board = this.DataContext as Board;

            this.InitializeCommands();
            this.InitializeComponent();
        }

        /* dependencies are in their own region/s later here */

        #endregion Constructors


        #region Initing Command properties

        private void InitializeCommands()  /* verified */  {
            InitWindowCommands();

            InitBasicItemCommands();   // includes copying to the clipboard, 1 of the 3 basic features of this app 
            InitItemMixingCommands();

            InitItemAddingCommands();  // includes adding from clipboard in two forms, 2 of the 3 basic features of this app 

            InitGroupingCommands();

            InitGroupedCopyAndCutCommands();
            InitGroupedStatusMutatingCommands();
            InitGroupedMutatingCommands();

            InitWholeBoardCommands();
        }


        #region Initing individual groups of Command properties

        /* if an initing method here is verified, it means as well 
         * that the workings of the Commands it inits are verified */

        private void InitWindowCommands()  /* verified */  {
            ToggleAlwaysOnTop = new Command(
                x => {
                    return (true);
                },
                x => {
                    this.Topmost = !this.Topmost;
                }
            );

            CloseSelf = new Command(
                x => { return (true); },
                x => {
                    // no saved-vs-unsaved checks done here 
                    this.Close();
                }
            );
        }

        private void InitBasicItemCommands()  /* verified */  {
            /* CopyItem is one of the three basic features of this app; the other two are InsertFromClipboard and InsertTextFromClipboard */

            CopyItem = new Command(
                x => {
                    // can't do this over empty new-item row even if Command is reachable then 
                    return (x is Item);
                },
                x => {
                    Item item = x as Item;
                    item.ToClipboard();  // copying 
                }
                );

            SplitItem = new Command(
                x => {
                    // can't do this over empty new-item row even if Command is reachable then, 
                    // and can't do this if no splitters in Item's content 
                    Item item = x as Item;

                    if (item == null) {
                        return (false);
                    }

                    return (item.CanSplit);
                },
                x => {
                    Item item = x as Item;

                    // saving original position to restore after splitting 
                    int at = this.BoardItems.SelectedIndex;

                    // splitting 
                    _board.SplitItem(item);

                    // restoring original position, lost when underlying collection changed 
                    this.BoardItems.SelectedIndex = at;
                }
            );

            DeleteItem = new Command(
                x => {
                    // can't do this over empty new-item row even if Command is reachable then 
                    return (x is Item);
                },
                x => {
                    Item item = x as Item;
                    _board.Remove(item);
                }
            );

            CutItem = new Command(
                x => {
                    // can't do this over empty new-item row even if Command is reachable then 
                    return (x is Item);
                },
                x => {
                    Item item = x as Item;

                    // cutting by copying and then deleting 
                    item.ToClipboard();
                    _board.Remove(item);
                }
                );

            ToggleHeader = new Command(
                x => { return (x is Item); },
                x => {
                    Item item = x as Item;
                    item.IsHeader = !item.IsHeader;  // toggling 
                }
            );

            ForgetItemHistory = new Command(
                x => {
                    // can't do this unless previously copied, Elvised in case reachable over empty new-item row 
                    Item item = x as Item;
                    return item?.WasCopied ?? false;
                },
                x => {
                    Item item = x as Item;
                    item.WasCopied = false;
                }
            );

            EditItem = new Command(
                x => {
                    // can't do this over empty new-item row even if Command is reachable then 
                    return (x is Item);
                },
                x => {
                    Item item = x as Item;

                    // editing: inits an EditItemWindow, which updates Item properties if saved, updating board; 
                    // returned value not needed here, because if cancelled, changes already discarded 
                    EditExitType exitType = item.Edit(EditType.EditItem);

                    // splitting when chosen in EditItemWindow 
                    if (exitType == EditExitType.SaveAndSplit) {
                        _board.SplitItem(item);
                    }
                }
            );
        }

        private void InitItemMixingCommands()  /* verified */  {
            CopyItemToStart = new Command(
                x => { return (x is Item); },
                x => {
                    Item item = x as Item;
                    item.ToClipboardStart();
                }
            );

            CopyItemToEnd = new Command(
                x => { return (x is Item); },
                x => {
                    Item item = x as Item;
                    item.ToClipboardEnd();
                }
            );

            CutItemToStart = new Command(
                x => {
                    // can't do this over empty new-item row even if Command is reachable then 
                    return (x is Item);
                },
                x => {
                    Item item = x as Item;

                    // cutting by copying and then deleting 
                    item.ToClipboardStart();
                    _board.Remove(item);
                }
            );

            CutItemToEnd = new Command(
                x => {
                    // can't do this over empty new-item row even if Command is reachable then 
                    return (x is Item);
                },
                x => {
                    Item item = x as Item;

                    // cutting by copying and then deleting 
                    item.ToClipboardEnd();
                    _board.Remove(item);
                }
            );

            AddAtItemStart = new Command(
                x => {
                    // can't do this over empty new-item row even if Command is reachable then 
                    return (x is Item);
                },
                x => {
                    Item item = x as Item;
                    item.AddFromClipboardAtStart();
                }
            );

            AddAtItemEnd = new Command(
                x => {
                    // can't do this over empty new-item row even if Command is reachable then 
                    return (x is Item);
                },
                x => {
                    Item item = x as Item;
                    item.AddFromClipboardAtEnd();
                }
            );
        }

        private void InitItemAddingCommands()  /* verified */  {
            /* InsertFromClipboard and InsertTextFromClipboard are two of the three basic features of this app; the other is CopyItem */

            InsertFromClipboard = new Command(
                x => {
                    return Item.CanInsertFromClipboard || Board.CanInsertItemsFromClipboard;
                },
                x => {
                    // inserts before Item or at end of Board / DataGrid if over empty new-row item or DataGrid's background space; 
                    // if no text, Item, or Item[] on clipboard, should not be available, but if available, does nothing 

                    int offset = this.SupplyCurrentOffset(x);

                    if (Clipboard.ContainsData(ClipboardFormats.QuickBoardItemArray)) {
                        this.InsertAnyClipboardItemArray(offset);
                    }
                    else {
                        this.InsertAnyClipboardItemOrText(offset);
                    }
                }
            );

            InsertTextFromClipboard = new Command(
                x => { return Item.CanInsertTextFromClipboard; },
                x => {
                    // inserts before Item or at end of Board / DataGrid if over empty new-row item or DataGrid's background space; 
                    // if no text on clipboard, should not be available, but if available, does nothing 

                    int offset = this.SupplyCurrentOffset(x);

                    if (Clipboard.ContainsText()) {
                        this.InsertAnyClipboardText(offset);
                    }
                }
            );

            AddNewItem = new Command(
                x => {
                    // always available 
                    return true;
                },
                x => {
                    // adds before clicked-on Item, or at end of Board / DataGrid if over empty new-row item or DataGrid's background space; 
                    // because Item being edited can be saved before exiting, but then discarded, it is cached and then removed if discarded 

                    int offset = this.SupplyCurrentOffset(x);
                    this.CacheEditableItemAndOffset(offset);

                    EditExitType exitType = this.ShowEditingWindowWired();
                    this.InsertSplitOrRemove(exitType);

                    this.ClearCachedItemAndOffset();
                }
            );
        }

        private void InitGroupingCommands()  /* verified */  {
            ToggleGrouped = new Command(
                x => {
                    return (x is Item);
                },
                x => {
                    Item item = x as Item;

                    // toggling based on current .IsGrouped 
                    bool doGroup = !item.IsGrouped;

                    if (doGroup) {
                        _board.AddToGrouped(item);
                    }
                    else {
                        _board.RemoveFromGrouped(item);
                    }
                }
            );

            ToggleStretchGrouped = new Command(
                x => {
                    // can't do this over empty new-item row even if Command is reachable then, 
                    // and there must be an existing set of grouped / selected items to stretch 
                    return (x is Item) && (_board.CountOfGrouped > 0);
                },
                x => {
                    /* stretches from nearest edge of grouped items */

                    Item item = x as Item;

                    // toggling based on current .IsGrouped 
                    bool doGroup = !item.IsGrouped;

                    int from = this.IndexToStretchFrom();
                    int to = _board.IndexOf(item);

                    List<Item> beingToggled = new List<Item>();

                    // ends swapped if stretching upwards 
                    if (from > to) {
                        int swap = from;
                        from = to;
                        to = swap;
                    }

                    // actually toggling; .AddToGrouped() only adds an Item once, so none repeated here 
                    for (int i = from; i <= to; i++) {
                        if (doGroup) {
                            _board.AddToGrouped(_board[i]);
                        }
                        else {
                            _board.RemoveFromGrouped(_board[i]);
                        }
                    }
                }
            );
        }

        private void InitGroupedCopyAndCutCommands()  /* verified */  {
            CopySelections = new Command(
                x => { return (_board.CountOfGrouped > 0); },
                x => { _board.CopyGroupedToClipboard(); }
            );

            CutSelections = new Command(
               x => { return (_board.CountOfGrouped > 0); },
               x => { _board.CutGroupedToClipboard(); }
           );

            CopySelectionsToStart = new Command(
                x => { return (_board.CountOfGrouped > 0); },
                x => { _board.CopyGroupedToClipboardStart(); }
            );

            CopySelectionsToEnd = new Command(
                x => { return (_board.CountOfGrouped > 0); },
                x => { _board.CopyGroupedToClipboardEnd(); }
            );

            CutSelectionsToStart = new Command(
                x => { return (_board.CountOfGrouped > 0); },
                x => { _board.CutGroupedToClipboardStart(); }
            );

            CutSelectionsToEnd = new Command(
                x => { return (_board.CountOfGrouped > 0); },
                x => { _board.CutGroupedToClipboardEnd(); }
            );
        }

        private void InitGroupedStatusMutatingCommands()  /* verified */  {
            ToggleHeaderOnSelections = new Command(
                x => { return (_board.CountOfGrouped > 0); },
                x => {
                    // not a strict toggle of each, but making all match toggling of first 
                    _board.ToggleHeaderOnGrouped();
                }
            );

            ForgetHistoryOfSelections = new Command(
                x => { return (_board.CountOfGrouped > 0); },
                x => {
                    // not a strict toggle of each, but making all match toggling of first 
                    _board.ClearWasCopiedOnGrouped();
                }
            );
        }

        private void InitGroupedMutatingCommands()  /* verified */  {
            MergeSelections = new Command(
               x => {
                   // merging is inapplicable when only one item is selected for grouped 
                   return (_board.CountOfGrouped > 1);
               },
               x => { _board.MergeGrouped(); }
            );

            UnselectSelections = new Command(
               x => {
                   // can deselect even if only one selection 
                   return (_board.CountOfGrouped > 0);
               },
               x => { _board.ClearGrouped(); }
            );

            MoveSelectionsToNewBoard = new Command(
                x => { return (_board.CountOfGrouped > 0); },
                x => {
                    Board newBoard = _board.GroupedToNewBoard();

                    // if not saved, :. null, no new app to spawn 
                    if (newBoard == null) {
                        return;
                    }

                    AppSpawner.SpawnFromBoard(newBoard);
                }
            );

            DeleteSelections = new Command(
                x => { return (_board.CountOfGrouped > 0); },
                x => { _board.RemoveGrouped(); }
            );
        }

        private void InitWholeBoardCommands()  /* verified */  {
            /* these act on the whole Board, although .Save and .SaveAs appear to act on window */

            Save = new Command(
                x => { return (_board.CanSave); },
                x => { _board.Save(); }
            );

            SaveAs = new Command(
                x => {
                    // Save As can always be invoked 
                    return (true);
                },
                x => { _board.SaveAs(); }
            );

            SelectAllItems = new Command(
                x => { return (_board.Count > 0); },
                x => {
                    _board.ClearGrouped();  // because Items could be added twice otherwise 
                    _board.AddToGrouped(_board.ToArray());
                }
            );

            CopyAllItems = new Command(
                x => { return (_board.Count > 0); },
                x => { _board.CopyAllItemsToClipboard(); }
            );

            ForgetBoardHistory = new Command(
                x => {
                    bool anyWereCopied = _board
                        .Any(n => n.WasCopied);

                    return anyWereCopied;
                },
                x => { _board.ClearAllWasCopiedFlags(); }
            );

            DeleteAllItems = new Command(
                x => { return (_board.Count > 0); },
                x => { _board.Clear(); }
            );
        }

        #endregion Initing individual groups of Command properties

        #endregion Initing Command properties


        #region Event handlers

        private void Window_Loaded(object sender, RoutedEventArgs e)  /* verified */  {
            // this is the first point at which .Top and .ActualHeight are available 
            this.FitWindowToScreenHeight();
        }

        #endregion Event handlers


        #region Private methods

        #region Dependencies of .InsertFromClipboard

        private void InsertAnyClipboardItemArray(int offset)  /* verified */  {
            _board.InsertItemsFromClipboard(offset);
            this.ScrollToInsertPoint(offset);
        }

        private void InsertAnyClipboardItemOrText(int offset)  /* verified */  {
            Item insertable = Item.FromClipboard();

            if (insertable == null) {
                return;
            }

            _board.Insert(offset, insertable);
            this.ScrollToInsertPoint(offset);
        }

        private void ScrollToInsertPoint(int offset)  /* verified */  {
            Item atInsertPoint = _board[offset];
            this.BoardItems.ScrollIntoView(atInsertPoint);
        }

        #endregion Dependencies of .InsertFromClipboard


        #region Dependencies of .InsertTextFromClipboard

        private void InsertAnyClipboardText(int offset)  /* verified */  {
            Item insertable = Item.FromClipboardText();

            if (insertable == null) {
                return;
            }

            _board.Insert(offset, insertable);
            this.ScrollToInsertPoint(offset);
        }

        #endregion Dependencies of .InsertTextFromClipboard


        #region Dependencies of .AddNewItem

        private int SupplyCurrentOffset(object arg)  /* verified */  {
            Item item = arg as Item;
            int offset = _board.Count;  // last Item in Board, rather than in DataGrid (which is after empty new-item row) 

            if (item != null) {
                offset = _board.IndexOf(item);
            }

            return offset;
        }

        private void CacheEditableItemAndOffset(int offset)  /* verified */  {
            // retaining these on the instance so they can be used in event handler 
            _cachedItem = new Item();
            _cachedOffset = offset;
        }

        private EditExitType ShowEditingWindowWired()  /* verified */  {
            // wiring event handler and having Item open the editing window 
            _cachedItem.PropertyChanged += this.CachedItemPropertyChanged;
            EditExitType editType = _cachedItem.Edit(EditType.AddItem);

            // unwiring event handler, which is used only when adding 
            _cachedItem.PropertyChanged -= this.CachedItemPropertyChanged;
            return editType;
        }

        private void InsertSplitOrRemove(EditExitType editType)  /* verified */  {
            // adding / splitting or discarding / removing based if choices at exit of EditItemWindow; 
            // the Item may have been saved already, so if exiting without a save, it must be removed 
            if (editType != EditExitType.Discard) {
                this.InsertCachedItemIfNotAlready();
            }
            else {
                _board.Remove(_cachedItem);  // doesn't throw Exception if no such Item 
            }

            if (editType == EditExitType.SaveAndSplit) {
                _board.SplitItem(_cachedItem);
            }
        }

        private void ClearCachedItemAndOffset()  /* verified */  {
            // clearing cache to default values 
            _cachedItem = null;
            _cachedOffset = -1;
        }

        /* dependency of ShowEditingWindowWired() */
        private void CachedItemPropertyChanged(object sender, PropertyChangedEventArgs e)  /* verified */  {
            if (e.PropertyName == nameof(_cachedItem.Content)) {
                this.InsertCachedItemIfNotAlready();
            }
        }

        /* dependency of InsertSplitOrRemove() */
        private void InsertCachedItemIfNotAlready()  /* verified */  {
            if (!_board.Contains(_cachedItem)) {
                _board.Insert(_cachedOffset, _cachedItem);
                this.BoardItems.ScrollIntoView(_cachedItem);
            }
        }

        #endregion Dependencies of .AddNewItem


        #region Dependencies of .ToggleStretchGrouped

        private int IndexToStretchFrom()  /* verified */  {
            /* the index to stretch from is the one of the last one grouped, so last Item in Board.Grouped */
            int topOfGrouped = _board.CountOfGrouped - 1;
            Item lastGrouped = _board.GroupedItem(topOfGrouped);
            int index = _board.IndexOf(lastGrouped);

            return (index);
        }

        #endregion Dependencies of .ToggleStretchGrouped


        private void FitWindowToScreenHeight()  /* verified */  {
            /* since SizeToContent="Height" stupidly allows the window to bleed off the screen, 
               coercing .Height to the screen's visible area, with a narrow open space below */

            double heightToSet = this.ActualHeight;

            double top = this.Top;
            double screenHeight = SystemParameters.WorkArea.Height;

            double maxHeight = screenHeight - top - SPACE_BELOW;

            if (this.ActualHeight >= maxHeight) {
                heightToSet = maxHeight;
            }

            // automatically sizing to content has to be turned off to control the height or width 
            this.SizeToContent = SizeToContent.Manual;
            this.Height = heightToSet;
        }

        #endregion Private methods

    }
}
