﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickBoard
{
    public static class ClipboardFormats
    {
        public static string QuickBoardItem { get; } = @"QuickBoardItem";
        public static string QuickBoardItemArray { get; } = @"QuickBoardItemArray";
    }
}
