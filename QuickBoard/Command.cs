﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace QuickBoard
{
    /* defines a Command-pattern -type object that can be customized using delegates 
     * or lambdas, so a numerous family of Command classes isn't necessary */

    public class Command : ICommand
    {
        #region Definitions, defined by ICommand

        public event EventHandler CanExecuteChanged  /* verified */  {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        #endregion Definitions, defined by ICommand


        #region Properties

        public Func<object, bool> CanEnact  /* ok */  { get; protected set; }
        public Action<object> Enact  /* ok */  { get; protected set; }

        #endregion Properties


        #region Constructors

        public Command(Func<object, bool> canEnact, Action<object> enact)  /* verified */  {
            this.CanEnact = canEnact;
            this.Enact = enact;
        }

        #endregion Constructors


        #region Public methods, defined by ICommand

        public bool CanExecute(object parameter)  /* passed */  {
            return (this.CanEnact(parameter));
        }

        public void Execute(object parameter)  /* passed */  {
            this.Enact(parameter);
        }

        #endregion Public methods, defined by ICommand
    }
}
