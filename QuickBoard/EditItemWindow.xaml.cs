﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;

namespace QuickBoard
{
    public partial class EditItemWindow : Window
    {
        #region Definitions

        readonly Dictionary<EditType, string> _editTypes
            = new Dictionary<EditType, string> { { EditType.AddItem, "Add New Item" }, { EditType.EditItem, "Edit Item" } };

        #endregion Definitions


        #region Fields

        Item _item;
        bool _isHeader;
        EditType _editType;
        EditExitType _exitType = EditExitType.Discard;  // setting to .Discard allows exit by X button without an uneditable empty Item 

        double _defaultHeight;
        double _defaultWidth;
        double _defaultFontSize;

        #endregion Fields


        #region Static Command properties

        public static Command ToggleWindowSize { get; protected set; }
        public static Command ToggleHeader { get; protected set; }
        public static Command SaveChanges { get; protected set; }
        public static Command SaveAndSplit { get; protected set; }
        public static Command ExitWithoutSaving { get; protected set; }

        #endregion Static Command properties


        #region Properties

        public Item Item { get => _item; set => _item = value; }
        public EditExitType ExitType { get => _exitType; set => _exitType = value; }

        #endregion Properties


        #region Constructors

        public EditItemWindow(Item item, EditType editType)  /* verified */  {
            _item = item;
            _isHeader = _item.IsHeader;
            _editType = editType;

            this.DataContext = _item;

            this.InitializeCommands();
            this.InitializeComponent();

            this.SetWindowTitle();
            this.SetHeaderButtonFace();
            this.SetFocusOnContent();
        }

        /* dependencies are in their own region/s later here */

        #endregion Constructors


        #region Initing Command properties

        /* if this initing method is verified, it means as well that the workings of the Commands it inits are verified */

        private void InitializeCommands()  /* verified */  {
            ToggleWindowSize = new Command(
                x => { return (true); },
                x => { ResizeWindowUpOrDown(); }
            );

            ToggleHeader = new Command(
                x => { return (true); },
                x => {
                    _isHeader = !_isHeader;
                    SetHeaderButtonFace();
                }
            );

            /* since SaveChanges does two things, it could be factored to two Commands, but I have kept it as 
             *     one because that's the only way to use it with a shifted option when bound to a Button */

            SaveChanges = new Command(
                x => {
                    // toggling on existence of changes isn't really worth it here 
                    return (true);
                },
                x => {
                    if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift)) {
                        SaveAndRemain();
                    }
                    else {
                        SaveAndExit();
                    }
                }
            );

            SaveAndSplit = new Command(
                x => { return (ContentDoesContainSplitter()); },
                x => {
                    /* save-and-split is the same as a save except for the exit type; 
                     * actual splitting is done after the window closes, on the Board */

                    this.SaveItem();

                    this.ExitType = EditExitType.SaveAndSplit;
                    this.Close();
                }
            );

            ExitWithoutSaving = new Command(
                x => { return (true); },
                x => {
                    this.ExitType = EditExitType.Discard;
                    this.Close();
                }
            );
        }

        #endregion Initing Command properties


        #region Other dependencies of constructors

        private void SetWindowTitle()  /* verified */  {
            this.Title = _editTypes[_editType];
        }

        private void SetHeaderButtonFace()  /* verified */  {
            /* this could be done with a DependencyProperty, but much more elaborately; 
             * consistency would then demand other, similar changes; not worth it here */

            this.HeaderButton.Content = (_isHeader ? "- Header" : "+ Header");
        }

        private void SetFocusOnContent()  /* verified */  {
            this.ItemContent.Focus();  // ugly, but must be done through code 
        }

        #endregion Other dependencies of constructors


        #region Dependencies of Commands

        private void ResizeWindowUpOrDown() {
            // resize upward and expand font, but only when already at minimum size 
            if (this.ActualHeight <= _defaultHeight && this.ActualWidth <= _defaultWidth) {
                this.Height *= 2;
                this.Width *= 2;
                this.ItemContent.FontSize *= 1.33;  // not expanded quite as much 

                // change button face 
                this.ResizeButton.Content = "-";
            }
            // whenever not at minimum size, resize downward to minimum size 
            else {
                this.Height = _defaultHeight;
                this.Width = _defaultWidth;

                // when resizing downward, shrink font proportionally, but never less than default 
                double fontReducedSize = this.ItemContent.FontSize /= 1.33;
                double fontSize = Math.Max(_defaultFontSize, fontReducedSize);
                this.ItemContent.FontSize = fontSize;

                // change button face 
                this.ResizeButton.Content = "+";
            }
        }

        private bool ContentDoesContainSplitter() {
            bool doesContainSplitter = false;

            foreach (string splitter in Item.Splitters) {
                doesContainSplitter |= this.ItemContent.Text.Contains(splitter);

                if (doesContainSplitter) {
                    break;
                }
            }

            return (doesContainSplitter);
        }


        #region Dependencies of .SaveChanges

        private void SaveAndRemain()  /* verified */  {
            this.SaveItem();
        }

        private void SaveAndExit()  /* verified */  {
            this.SaveItem();

            this.ExitType = EditExitType.Save;
            this.Close();
        }

        private void SaveItem()  /* verified */  {
            // setting this property manually allows one-way binding; 
            // fires _item's PropertyChanged to automatically update main window 
            _item.Content = this.ItemContent.Text;
            _item.IsHeader = _isHeader;
        }

        #endregion Dependencies of SaveChanges

        #endregion Dependencies of Commands


        #region Event handlers and dependencies

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            // set size values used when toggling size up or down, now that these values are available 
            this.SetSizingDefaults();
        }

        /* dependency of Window_Loaded() */
        private void SetSizingDefaults()  /* verified */  {
            _defaultHeight = this.ActualHeight;
            _defaultWidth = this.ActualWidth;
            _defaultFontSize = this.ItemContent.FontSize;
        }

        private void ItemContent_MouseWheel(object sender, MouseWheelEventArgs e)  /* verified */  {
            // scale the font up or down based on mouse wheel

            if (Keyboard.Modifiers == ModifierKeys.Control) {
                if (e.Delta > 0) {
                    this.ItemContent.FontSize += 0.5;
                }
                else if (e.Delta < 0) {
                    this.ItemContent.FontSize -= 0.5;
                }
            }
        }

        #endregion Event handlers and dependencies

    }
}
