﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows;
using System.Xml.Serialization;

namespace QuickBoard
{
    public enum EditExitType
    {
        Save,
        Discard,
        SaveAndSplit,
    }
}