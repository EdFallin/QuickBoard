﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using QuickBoard;

namespace QuickBoardTests
{
    [TestClass]
    public class CommandTests
    {
        #region Fields

        private bool _canEnact = false;
        private bool _didEnact = false;
        private object _arg = null;

        #endregion Fields


        #region Fixtures

        [TestCleanup]
        public void AfterEachTest()  /* verified */  {
            _canEnact = false;
            _didEnact = false;
            _arg = null;
        }

        #endregion Fixtures


        #region CanExecute()

        [TestMethod()]
        public void CanExecute__Known_Injected_Code__Assert_Does_Execute_Setting_Flag()  /* working */  {
            /* to ensure that the injected code runs, I'm just checking that it performs some chosen operation; 
             * return values in real use would depend on specific code injected */

            //**  groundwork  **//
            Func<object, bool> canEnact = (x => {
                _canEnact = true;
                return (_canEnact);
            });

            Action<object> enactThis = (x => { /* no operations */ });  // ops not needed for this test 

            Command subject = new Command(canEnact, enactThis);


            //**  pre-testing  **//
            Assert.AreEqual(false, _canEnact);


            //**  exercising the code  **//
            subject.CanExecute(null);  // arg not used, and return value not used 


            //**  testing  **//
            Assert.AreEqual(true, _canEnact);
        }

        [TestMethod()]
        public void CanExecute__Known_Arg__Assert_Does_Get_Arg_Setting_Field()  /* working */  {
            /* to ensure that the injected code receives the @parameter arg, I use code that just sets the test field */

            //**  groundwork  **//
            Window arg = new Window();

            Func<object, bool> canEnact = (x => {
                _arg = x;
                return (true);
            });

            Action<object> enactThis = (x => { /* no operations */ });  // ops not needed for this test 

            Command subject = new Command(canEnact, enactThis);

            object expected = arg;


            //**  pre-testing  **//
            Assert.AreEqual(null, _arg);


            //**  exercising the code  **//
            subject.CanExecute(arg);  // return value not used 


            //**  testing  **//
            Assert.AreEqual(expected, _arg);
        }

        #endregion CanExecute()


        #region Execute()

        [TestMethod()]
        public void Execute__Known_Injected_Code__Assert_Does_Execute_Setting_Flag()  /* working */  {
            /* to ensure that the injected code runs, I'm just checking that it performs some chosen operation */

            //**  groundwork  **//
            Func<object, bool> canEnact = (x => { return (false); });  // value not automatically checked 
            Action<object> enactThis = (x => { _didEnact = true; });

            Command subject = new Command(canEnact, enactThis);


            //**  pre-testing  **//
            Assert.AreEqual(false, _didEnact);


            //**  exercising the code  **//
            subject.Execute(null);  // arg not used; no return value 


            //**  testing  **//
            Assert.AreEqual(true, _didEnact);
        }

        [TestMethod()]
        public void Execute__Known_Arg__Assert_Does_Get_Arg_Setting_Field()  /* working */  {
            /* to ensure that the injected code receives the @parameter arg, I use code that just sets the test field */

            //**  groundwork  **//
            Window arg = new Window();

            Func<object, bool> canEnact = (x => { return (false); });  // value not automatically checked 
            Action<object> enactThis = (x => { _arg = x; });

            Command subject = new Command(canEnact, enactThis);

            object expected = arg;


            //**  pre-testing  **//
            Assert.AreEqual(null, _arg);


            //**  exercising the code  **//
            subject.Execute(arg);  // no return value 


            //**  testing  **//
            Assert.AreEqual(expected, _arg);
        }

        #endregion Execute()
    }
}
