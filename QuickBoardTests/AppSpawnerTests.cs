﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using QuickBoard;

namespace QuickBoardTests
{
    [TestClass]
    public class AppSpawnerTests
    {
        #region AsSafeArg()

        [TestMethod()]
        public void AsSafeArg__Already_Quotes__Assert_No_Changes()  /* working */  {
            //**  groundwork  **//
            PrivateType t = new PrivateType(typeof(AppSpawner));
            string arg = "\"already has quotes\"";

            string expected = arg;


            //**  exercising the code  **//
            string actual = (string)t.InvokeStatic("AsSafeArg", arg);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void AsSafeArg__No_Quotes__Assert_Quotes_Added()  /* working */  {
            //**  groundwork  **//
            PrivateType t = new PrivateType(typeof(AppSpawner));
            string arg = "has no quotes at start";

            string expected = $"\"{ arg }\"";


            //**  exercising the code  **//
            string actual = (string)t.InvokeStatic("AsSafeArg", arg);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion AsSafeArg()

    }
}
