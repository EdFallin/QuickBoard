﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QuickBoard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace QuickBoardTests
{
    [TestClass()]
    public class ItemTests
    {
        #region Fields

        bool contentChanged = false;
        bool isEmptyChanged = false;
        bool wasCopiedChanged = false;

        #endregion Fields


        #region Fixtures

        [TestInitialize]
        public void BeforeEachTest() {
            contentChanged = false;
            isEmptyChanged = false;
            wasCopiedChanged = false;
        }

        public void WhenPropertyChanged(object sender, PropertyChangedEventArgs arg) {
            Item reference = Item.Empty;

            if (arg.PropertyName == nameof(reference.Content)) {
                contentChanged = true;
            }

            if (arg.PropertyName == nameof(reference.IsEmpty)) {
                isEmptyChanged = true;
            }

            if (arg.PropertyName == nameof(reference.WasCopied)) {
                wasCopiedChanged = true;
            }
        }

        #endregion Fixtures


        #region .IsEmpty

        [TestMethod()]
        public void IsEmpty__Is_Null__Assert_True()  /* working */  {
            //**  groundwork  **//
            Item item = new Item();  // null _content 


            //**  exercising the code  **//
            bool actual = item.IsEmpty;


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        [TestMethod()]
        public void IsEmpty__White_Space__Assert_False()  /* working */  {
            //**  groundwork  **//
            Item item = new Item(string.Empty);


            //**  exercising the code  **//
            bool actual = item.IsEmpty;


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void IsEmpty__Has_Text__Assert_False()  /* working */  {
            //**  groundwork  **//
            Item item = new Item(string.Empty);


            //**  exercising the code  **//
            bool actual = item.IsEmpty;


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        #endregion .IsEmpty


        #region .CanSplit

        [TestMethod()]
        public void CanSplit__Is_Null__Assert_False()  /* working */  {
            //**  groundwork  **//
            Item item = new Item();  // null _content 


            //**  exercising the code  **//
            bool actual = item.CanSplit;


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void CanSplit__No_Splitters__Assert_False()  /* working */  {
            //**  groundwork  **//
            Item item = new Item("no splitters");


            //**  exercising the code  **//
            bool actual = item.CanSplit;


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void CanSplit__Return_New_Present__Assert_True()  /* working */  {
            //**  groundwork  **//
            // \r\n is a splitter; no leading ×@ here, because then \s treated as literals, not escape sequences 
            Item item = new Item("before\r\nafter");


            //**  exercising the code  **//
            bool actual = item.CanSplit;


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        [TestMethod()]
        public void CanSplit__New_Present__Assert_True()  /* working */  {
            //**  groundwork  **//
            // \n is a splitter; no leading ×@ here, because then \s treated as literals, not escape sequences 
            Item item = new Item("before\nafter");


            //**  exercising the code  **//
            bool actual = item.CanSplit;


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        #endregion .CanSplit


        #region OnPropertyChanged() on .Content

        [TestMethod()]
        public void OnPropertyChanged__Content__Changed_From_Null__Assert_Both_Callbacks()  /* working */  {
            //**  groundwork  **//
            Item item = new Item(null);
            item.PropertyChanged += WhenPropertyChanged;


            //**  exercising the code  **//
            item.Content = "text";


            //**  testing  **//
            Assert.AreEqual(true, contentChanged);
            Assert.AreEqual(true, isEmptyChanged);
        }

        [TestMethod()]
        public void OnPropertyChanged__Content__Changed_To_Null__Assert_Both_Callbacks()  /* working */  {
            //**  groundwork  **//
            Item item = new Item("text");
            item.PropertyChanged += WhenPropertyChanged;


            //**  exercising the code  **//
            item.Content = null;


            //**  testing  **//
            Assert.AreEqual(true, contentChanged);
            Assert.AreEqual(true, isEmptyChanged);
        }

        [TestMethod()]
        public void OnPropertyChanged__Content__Changed_No_Nulls__Assert_Content_Callback()  /* working */  {
            //**  groundwork  **//
            Item item = new Item("text");
            item.PropertyChanged += WhenPropertyChanged;


            //**  exercising the code  **//
            item.Content = "writing";


            //**  testing  **//
            Assert.AreEqual(true, contentChanged);
            Assert.AreEqual(false, isEmptyChanged);
        }

        [TestMethod()]
        public void OnPropertyChanged__Content__Changed_Both_Whitespace__Assert_Content_Callback()  /* working */  {
            //**  groundwork  **//
            Item item = new Item(" ");
            item.PropertyChanged += WhenPropertyChanged;


            //**  exercising the code  **//
            item.Content = "\t";


            //**  testing  **//
            Assert.AreEqual(true, contentChanged);
            Assert.AreEqual(false, isEmptyChanged);
        }

        [TestMethod()]
        public void OnPropertyChanged__Content__Changed_One_Empty__Assert_Content_Callback()  /* working */  {
            //**  groundwork  **//
            Item item = new Item(" ");
            item.PropertyChanged += WhenPropertyChanged;


            //**  exercising the code  **//
            item.Content = string.Empty;


            //**  testing  **//
            Assert.AreEqual(true, contentChanged);
            Assert.AreEqual(false, isEmptyChanged);
        }

        #endregion OnPropertyChanged() on .Content


        #region OnPropertyChanged() on .WasCopied

        [TestMethod()]
        public void OnPropertyChanged__WasCopied__Changed__Assert_Callback()  /* working */  {
            //**  groundwork  **//
            Item item = new Item(null);
            item.PropertyChanged += WhenPropertyChanged;


            //**  exercising the code  **//
            item.WasCopied = true;


            //**  testing  **//
            Assert.AreEqual(true, wasCopiedChanged);
        }

        #endregion OnPropertyChanged() on .WasCopied


        #region ToClipboard()

        [TestMethod()]
        public void ToClipboard__Null_Content__Assert_Clipboard_Unchanged()  /* working */  {
            /* if this test fails, you'll see a message box onscreen; 
             * unfortunately, I can't catch the Exception in my method 
             * _and_ use it to coerce a fail here */

            //**  groundwork  **//
            string expected = "unchanged";
            Clipboard.SetText(expected);
            Thread.Sleep(1);

            Item item = new Item();  // null .Content 


            //**  exercising the code  **//
            item.ToClipboard();


            //**  getting the results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetText();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ToClipboard__Non_Null_Content__Assert_On_Clipboard()  /* working */  {
            //**  groundwork  **//
            Clipboard.SetText("test failed");
            Thread.Sleep(1);

            string expected = "content";
            Item item = new Item(expected);


            //**  exercising the code  **//
            item.ToClipboard();


            //**  getting the results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetText();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ToClipboard__Valid_Item__Assert_Clipboard_Data_Not_Null()  /* working */  {
            //**  groundwork  **//
            Clipboard.SetText("test failed");
            Thread.Sleep(1);

            string content = "content";
            Item item = new Item(content, true);


            //**  exercising the code  **//
            item.ToClipboard();


            //**  getting the results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetData(ClipboardFormats.QuickBoardItem) as string;


            //**  testing  **//
            // contents of @actual are in effect tested elsewhere 
            Assert.AreNotEqual(null, actual);
        }

        #endregion ToClipboard()


        #region Serialize()

        [TestMethod()]
        public void Serialize__Known_Content_And_IsHeader_True__Assert_Both_Present_In_Xml()  /* working */  {
            //**  groundwork  **//
            string content = "This is the Item's content.";
            Item item = new Item(content, true);

            string expContentNode = $"<Content>{ content }</Content>";
            string expHeaderNode = $"<IsHeader>true</IsHeader>";


            //**  exercising the code  **//
            string actual = item.Serialize();


            //**  testing  **//
            StringAssert.Contains(actual, expContentNode);
            StringAssert.Contains(actual, expHeaderNode);
        }

        [TestMethod()]
        public void Serialize__Known_Content_And_IsHeader_False__Assert_Both_Present_In_Xml()  /* working */  {
            //**  groundwork  **//
            string content = "This is the Item's content.";
            Item item = new Item(content, false);

            string expContentNode = $"<Content>{ content }</Content>";
            string expHeaderNode = $"<IsHeader>false</IsHeader>";


            //**  exercising the code  **//
            string actual = item.Serialize();


            //**  testing  **//
            StringAssert.Contains(actual, expContentNode);
            StringAssert.Contains(actual, expHeaderNode);
        }

        [TestMethod()]
        public void Serialize__Knowns__Assert_Non_Serialized_Properties_Not_In_Xml()  /* working */  {
            //**  groundwork  **//
            string content = "This is the Item's content.";
            Item item = new Item(content, true);

            // the .IsGrouped and .WasCopied properties should not be serialized; 
            // if not serialized, their property names are not in the output XML 
            Regex expNotIsGroupedTag = new Regex("IsGrouped");
            Regex expNotWasCopiedTag = new Regex("WasCopied");


            //**  exercising the code  **//
            string actual = item.Serialize();


            //**  testing  **//
            // DoesNotMatch() looks for regex match, which might be part _or_ all of text 
            StringAssert.DoesNotMatch(actual, expNotIsGroupedTag);
            StringAssert.DoesNotMatch(actual, expNotWasCopiedTag);
        }

        #endregion Serialize()


        #region ToClipboardStart()

        [TestMethod()]
        public void ToClipboardStart__Null_Content__Assert_Clipboard_Unchanged()  /* working */  {
            /* if this test fails, you'll see a message box onscreen; 
             * unfortunately, I can't catch the Exception in my method 
             * _and_ use it to coerce a fail here */

            //**  groundwork  **//
            string expected = "unchanged";
            Clipboard.SetText(expected);
            Thread.Sleep(1);

            Item item = new Item();  // null .Content 


            //**  exercising the code  **//
            item.ToClipboardStart();


            //**  getting the results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetText();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ToClipboardStart__Non_Null_Content__Assert_On_Clipboard()  /* working */  {
            //**  groundwork  **//
            Clipboard.SetText("original text");
            Thread.Sleep(1);

            string adding = "content";

            Item item = new Item(adding);

            string expected = "content\r\noriginal text";


            //**  exercising the code  **//
            item.ToClipboardStart();


            //**  getting the results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetText();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion ToClipboardStart()


        #region ToClipboardEnd()

        [TestMethod()]
        public void ToClipboardEnd__Null_Content__Assert_Clipboard_Unchanged()  /* working */  {
            /* if this test fails, you'll see a message box onscreen; 
             * unfortunately, I can't catch the Exception in my method 
             * _and_ use it to coerce a fail here */

            //**  groundwork  **//
            string expected = "unchanged";
            Clipboard.SetText(expected);
            Thread.Sleep(1);

            Item item = new Item();  // null .Content 


            //**  exercising the code  **//
            item.ToClipboardEnd();


            //**  getting the results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetText();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ToClipboardEnd__Non_Null_Content__Assert_On_Clipboard()  /* working */  {
            //**  groundwork  **//
            Clipboard.SetText("original text");
            Thread.Sleep(1);

            string adding = "content";

            Item item = new Item(adding);

            string expected = "original text\r\ncontent";


            //**  exercising the code  **//
            item.ToClipboardEnd();


            //**  getting the results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetText();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion ToClipboardEnd()


        #region .CanInsertFromClipboard

        [TestMethod()]
        public void CanInsertFromClipboard__Clipboard_Empty__Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            Clipboard.Clear();
            Thread.Sleep(1);

            bool expected = false;


            //**  exercising the code  **//
            bool actual = Item.CanInsertFromClipboard;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CanInsertFromClipboard__Text_On_Clipboard__Assert_Returns_True()  /* working */  {
            //**  groundwork  **//
            Clipboard.SetText("item text");
            Thread.Sleep(1);

            bool expected = true;


            //**  exercising the code  **//
            bool actual = Item.CanInsertFromClipboard;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CanInsertFromClipboard__Item_On_Clipboard__Assert_Returns_True()  /* working */  {
            //**  groundwork  **//
            Item item = new Item("item content", true);
            item.ToClipboard();
            Thread.Sleep(1);

            bool expected = true;


            //**  exercising the code  **//
            bool actual = Item.CanInsertFromClipboard;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion .CanInsertFromClipboard


        #region .CanInsertTextFromClipboard

        [TestMethod()]
        public void CanInsertTextFromClipboard__Clipboard_Empty__Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            Clipboard.Clear();
            Thread.Sleep(1);

            bool expected = false;


            //**  exercising the code  **//
            bool actual = Item.CanInsertTextFromClipboard;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CanInsertTextFromClipboard__Text_On_Clipboard__Assert_Returns_True()  /* working */  {
            //**  groundwork  **//
            Clipboard.SetText("item text");
            Thread.Sleep(1);

            bool expected = true;


            //**  exercising the code  **//
            bool actual = Item.CanInsertTextFromClipboard;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion .CanInsertTextFromClipboard


        #region FromClipboard()

        [TestMethod()]
        public void FromClipboard__Empty_Clipboard__Assert_Returns_Null()  /* working */  {
            //**  groundwork  **//
            Clipboard.Clear();
            Thread.Sleep(1);


            //**  exercising the code  **//
            Item actual = Item.FromClipboard();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(null, actual);
        }

        [TestMethod()]
        public void FromClipboard__Whitespace_Clipboard_Text__Assert_Correct_Item_Content()  /* working */  {
            //**  groundwork  **//
            string expected = "   \t  \r\n  \r ";
            Clipboard.SetText(expected);
            Thread.Sleep(1);


            //**  exercising the code  **//
            Item actual = Item.FromClipboard();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(expected, actual?.Content);
        }

        [TestMethod()]
        public void FromClipboard__Valid_Text__Assert_Correct_Item_Content()  /* working */  {
            //**  groundwork  **//
            string expected = "clipboard text";
            Clipboard.SetText(expected);
            Thread.Sleep(1);


            //**  exercising the code  **//
            Item actual = Item.FromClipboard();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(expected, actual?.Content);
        }

        [TestMethod()]
        public void FromClipboard__Valid_Xml_IsHeader_True__Assert_Correct_Item_Properties()  /* working */  {
            //**  groundwork  **//
            string expected = "clipboard text";
            Item item = new Item(expected, true);
            string asXml = item.Serialize();

            DataObject asData = new DataObject(ClipboardFormats.QuickBoardItem, asXml);
            Clipboard.SetDataObject(asData, true);
            Thread.Sleep(1);


            //**  exercising the code  **//
            Item actual = Item.FromClipboard();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(expected, actual?.Content);
            Assert.AreEqual(true, actual?.IsHeader ?? false);  // anti-default 
        }

        [TestMethod()]
        public void FromClipboard__Valid_Xml_IsHeader_False__Assert_Correct_Item_Properties()  /* working */  {
            //**  groundwork  **//
            string expected = "clipboard text";
            Item item = new Item(expected, false);
            string asXml = item.Serialize();

            DataObject asData = new DataObject(ClipboardFormats.QuickBoardItem, asXml);
            Clipboard.SetDataObject(asData, true);
            Thread.Sleep(1);


            //**  exercising the code  **//
            Item actual = Item.FromClipboard();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(expected, actual?.Content);
            Assert.AreEqual(false, actual?.IsHeader ?? false);  // anti-default 
        }

        [TestMethod()]
        public void FromClipboard__Valid_Item_With_ToClipboard_IsHeader_True__Assert_Correct_Item_Properties()  /* working */  {
            //**  groundwork  **//
            string expected = "clipboard text";
            Item item = new Item(expected, true);
            item.ToClipboard();
            Thread.Sleep(1);


            //**  exercising the code  **//
            Item actual = Item.FromClipboard();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(expected, actual?.Content);
            Assert.AreEqual(true, actual?.IsHeader ?? false);  // anti-default 
        }

        [TestMethod()]
        public void FromClipboard__Valid_Item_With_ToClipboard_IsHeader_False__Assert_Correct_Item_Properties()  /* working */  {
            //**  groundwork  **//
            string expected = "clipboard text";
            Item item = new Item(expected, false);
            item.ToClipboard();
            Thread.Sleep(1);


            //**  exercising the code  **//
            Item actual = Item.FromClipboard();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(expected, actual?.Content);
            Assert.AreEqual(false, actual?.IsHeader ?? true);  // anti-default 
        }

        #endregion FromClipboard()


        #region FromClipboardText()

        [TestMethod()]
        public void FromClipboardText__Empty_Clipboard__Assert_Returns_Null()  /* working */  {
            //**  groundwork  **//
            Clipboard.Clear();
            Thread.Sleep(1);


            //**  exercising the code  **//
            Item actual = Item.FromClipboardText();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(null, actual);
        }

        [TestMethod()]
        public void FromClipboardText__Item_Only__Assert_Returns_Null()  /* working */  {
            //**  groundwork  **//
            string expected = "clipboard text";
            Item item = new Item(expected, true);
            string asXml = item.Serialize();

            // no plain text on clipboard 
            DataObject asData = new DataObject(ClipboardFormats.QuickBoardItem, asXml);
            Clipboard.SetDataObject(asData, true);
            Thread.Sleep(1);


            //**  exercising the code  **//
            Item actual = Item.FromClipboardText();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(null, actual);
        }

        [TestMethod()]
        public void FromClipboardText__Whitespace_Clipboard_Text__Assert_Correct_Item_Content()  /* working */  {
            //**  groundwork  **//
            string expected = "   \t  \r\n  \r ";
            Clipboard.SetText(expected);
            Thread.Sleep(1);


            //**  exercising the code  **//
            Item actual = Item.FromClipboardText();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(expected, actual?.Content);
        }

        [TestMethod()]
        public void FromClipboardText__Valid_Text__Assert_Correct_Item_Content()  /* working */  {
            //**  groundwork  **//
            string expected = "clipboard text";
            Clipboard.SetText(expected);
            Thread.Sleep(1);


            //**  exercising the code  **//
            Item actual = Item.FromClipboardText();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(expected, actual?.Content);
        }

        #endregion FromClipboardText()


        #region FromSerial()

        [TestMethod()]
        public void FromSerial__Null_Arg__Assert_Returns_Null()  /* working */  {
            //**  groundwork  **//
            string arg = null;


            //**  exercising the code  **//
            Item actual = Item.FromSerial(arg);


            //**  testing  **//
            Assert.AreEqual(null, actual);
        }

        [TestMethod()]
        public void FromSerial__Not_Xml__Assert_Returns_Null()  /* working */  {
            //**  groundwork  **//
            string arg = "not xml";


            //**  exercising the code  **//
            Item actual = Item.FromSerial(arg);


            //**  testing  **//
            Assert.AreEqual(null, actual);
        }

        [TestMethod()]
        public void FromSerial__Invalid_Xml__Assert_Returns_Null()  /* working */  {
            //**  groundwork  **//
            // generating XML that is valid, but not for an Item 
            List<int> toSerialize = new List<int> { 3, 4, 5, 6, 7 };
            StringBuilder asBuilder = new StringBuilder();

            using (StringWriter writer = new StringWriter(asBuilder)) {
                XmlSerializer serializer = new XmlSerializer(typeof(List<int>));
                serializer.Serialize(writer, toSerialize);
            }

            string arg = asBuilder.ToString();


            //**  exercising the code  **//
            Item actual = Item.FromSerial(arg);


            //**  testing  **//
            Assert.AreEqual(null, actual);
        }

        [TestMethod()]
        public void FromSerial__Valid_Xml__Assert_Item_With_Correct_Properties()  /* working */  {
            //**  groundwork  **//
            // generating XML that is valid for an Item 
            Item toSerialize = new Item("item content", true);
            StringBuilder asBuilder = new StringBuilder();

            using (StringWriter writer = new StringWriter(asBuilder)) {
                XmlSerializer serializer = new XmlSerializer(typeof(Item));
                serializer.Serialize(writer, toSerialize);
            }

            string arg = asBuilder.ToString();

            string expContent = toSerialize.Content;


            //**  exercising the code  **//
            Item actual = Item.FromSerial(arg);


            //**  testing  **//
            Assert.AreNotEqual(null, actual);
            Assert.AreEqual(expContent, actual?.Content);
            Assert.AreEqual(true, actual?.IsHeader ?? false);  // anti-default 
        }

        #endregion FromSerial()


        #region Split()

        [TestMethod()]
        public void Split__Not_Splittable__Assert_Original_Unchanged()  /* working */  {
            //**  groundwork  **//
            string text = "this text is not splittable at hard returns";
            Item item = new Item(text);

            string expected = text;  // a copy 


            //**  exercising the code  **//
            item.Split();  // return value ignored 


            //**  gathering results  **//
            string actual = item.Content;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Split__Not_Splittable__Assert_Single_Listed_Output_Item_Same_As_Original()  /* working */  {
            //**  groundwork  **//
            string text = "this text is not splittable at hard returns";
            Item item = new Item(text);

            Item expected = item;  // a reference 


            //**  exercising the code  **//
            List<Item> actuals = item.Split();


            //**  testing  **//
            Assert.AreEqual(1, actuals.Count);

            Item actual = actuals[0];
            Assert.AreEqual(expected, actual);  // reference equality 
        }

        [TestMethod()]
        public void Split__Splittable__Assert_Correct_Output_Items()  /* working */  {
            //**  groundwork  **//
            string[] splitters = { "\r\n" };
            string text = "this text is\r\nsplittable\r\nat hard returns";

            Item item = new Item(text);

            string[] expecteds = text.Split(splitters, StringSplitOptions.RemoveEmptyEntries);
            Item expected = item;  // a reference 


            //**  exercising the code  **//
            List<Item> actuals = item.Split();


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actuals.Count);  // should be 3 

            Item actual = actuals[0];
            Assert.AreEqual(expected, actual);  // reference equality 

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], actuals[i].Content);
            }
        }

        [TestMethod()]
        public void Split__Splittable_Linefeeds_Only__Assert_Correct_Output_Items()  /* working */  {
            /* text saved in XML is probably split with @\n only */

            //**  groundwork  **//
            string[] splitters = { "\n" };
            string text = "this text is\nsplittable\nat line feeds";

            Item item = new Item(text);

            string[] expecteds = text.Split(splitters, StringSplitOptions.RemoveEmptyEntries);
            Item expected = item;  // a reference 


            //**  exercising the code  **//
            List<Item> actuals = item.Split();


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actuals.Count);  // should be 3 

            Item actual = actuals[0];
            Assert.AreEqual(expected, actual);  // reference equality 

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], actuals[i].Content);
            }
        }

        [TestMethod()]
        public void Split__Splittable__Assert_Correct_Remaining_Text_In_Original()  /* working */  {
            //**  groundwork  **//
            string[] splitters = { "\r\n" };
            string text = "this text is\r\nsplittable\r\nat hard returns";

            Item item = new Item(text);

            string[] expecteds = text.Split(splitters, StringSplitOptions.RemoveEmptyEntries);
            string expected = expecteds[0];


            //**  exercising the code  **//
            item.Split();  // return value ignored 


            //**  gathering results  **//
            string actual = item.Content;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void Split__Splittable_IsHeader_True__Assert_All_Output_Items_IsHeader_True()  /* working */  {
            //**  groundwork  **//
            string[] splitters = { "\r\n" };
            string text = "this text is\r\nsplittable\r\nat hard returns";

            Item item = new Item(text);
            item.IsHeader = true;

            string[] expecteds = text.Split(splitters, StringSplitOptions.RemoveEmptyEntries);
            string expected = expecteds[0];


            //**  exercising the code  **//
            List<Item> actuals = item.Split();


            //**  testing  **//
            foreach (Item actual in actuals) {
                Assert.AreEqual(true, actual.IsHeader);
            }
        }

        [TestMethod()]
        public void Split__Splittable_IsHeader_False__Assert_All_Output_Items_IsHeader_False()  /* working */  {
            //**  groundwork  **//
            string[] splitters = { "\r\n" };
            string text = "this text is\r\nsplittable\r\nat hard returns";

            Item item = new Item(text);
            item.IsHeader = false;

            string[] expecteds = text.Split(splitters, StringSplitOptions.RemoveEmptyEntries);
            string expected = expecteds[0];


            //**  exercising the code  **//
            List<Item> actuals = item.Split();


            //**  testing  **//
            foreach (Item actual in actuals) {
                Assert.AreEqual(false, actual.IsHeader);
            }
        }

        #endregion Split()


        #region FromItems()

        [TestMethod()]
        public void FromItems__One_Item__Assert_Returns_Equivalent()  /* working */  {
            //**  groundwork  **//
            Item item = new Item("Single item");
            item.IsHeader = true;
            List<Item> arg = new List<Item> { item };

            string expContent = item.Content;
            bool expIsHeader = item.IsHeader;


            //**  exercising the code  **//
            Item actual = Item.FromItems(arg);


            //**  testing  **//
            Assert.AreEqual(expContent, actual.Content);
            Assert.AreEqual(expIsHeader, actual.IsHeader);
        }

        [TestMethod()]
        public void FromItems__Several__Assert_Returns_Correct_Merged_Text()  /* working */  {
            //**  groundwork  **//
            Item item1 = new Item("First item");
            Item item2 = new Item("Second item");
            Item item3 = new Item("Third item");

            List<Item> arg = new List<Item> { item1, item2, item3 };

            string expected = "First item\r\nSecond item\r\nThird item";


            //**  exercising the code  **//
            Item actual = Item.FromItems(arg);


            //**  testing  **//
            Assert.AreEqual(expected, actual.Content);
        }

        [TestMethod()]
        public void FromItems__Several_Mixed_Returns__Assert_Returns_Correct_Merged_Text()  /* working */  {
            //**  groundwork  **//
            Item item1 = new Item("First item\n\n\n");
            Item item2 = new Item("Second item\r\n");
            Item item3 = new Item("Third item\r\n");

            List<Item> arg = new List<Item> { item1, item2, item3 };

            string expected = "First item\n\n\r\nSecond item\r\nThird item\r\n";


            //**  exercising the code  **//
            Item actual = Item.FromItems(arg);


            //**  testing  **//
            Assert.AreEqual(expected, actual.Content);
        }

        [TestMethod()]
        public void FromItems__Several_First_Not_Header__Assert_Returned_Not_Header()  /* working */  {
            //**  groundwork  **//
            Item item1 = new Item("First item");
            Item item2 = new Item("Second item");
            Item item3 = new Item("Third item");

            item1.IsHeader = false;
            List<Item> arg = new List<Item> { item1, item2, item3 };


            //**  exercising the code  **//
            Item actual = Item.FromItems(arg);


            //**  testing  **//
            Assert.AreEqual(false, actual.IsHeader);
        }

        [TestMethod()]
        public void FromItems__First_Is_Header__Assert_Returned_Is_Header()  /* working */  {
            //**  groundwork  **//
            Item item1 = new Item("First item");
            Item item2 = new Item("Second item");
            Item item3 = new Item("Third item");

            item1.IsHeader = true;
            List<Item> arg = new List<Item> { item1, item2, item3 };


            //**  exercising the code  **//
            Item actual = Item.FromItems(arg);


            //**  testing  **//
            Assert.AreEqual(true, actual.IsHeader);
        }

        #endregion FromItems()


        #region RemoveTrailingLineBreak()

        [TestMethod()]
        public void RemoveTrailingLineBreak__Ends_Without_Line_Breaks__Assert_Nothing_Removed()  /* working */  {
            //**  groundwork  **//
            Item item = new Item();
            PrivateType pt = new PrivateType(typeof(Item));

            string text = "no trailing line break";
            string expected = text;


            //**  exercising the code  **//
            string actual = (string)pt.InvokeStatic("RemoveTrailingLineBreak", text);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveTrailingLineBreak__Ends_With_NRN__Assert_Only_RN_Removed()  /* working */  {
            //**  groundwork  **//
            Item item = new Item();
            PrivateType pt = new PrivateType(typeof(Item));

            string text = "trailing NRN line breaks\n\r\n";
            string expected = "trailing NRN line breaks\n";


            //**  exercising the code  **//
            string actual = (string)pt.InvokeStatic("RemoveTrailingLineBreak", text);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveTrailingLineBreak__Ends_With_RNN__Assert_Only_Last_N_Removed()  /* working */  {
            //**  groundwork  **//
            Item item = new Item();
            PrivateType pt = new PrivateType(typeof(Item));

            string text = "trailing RNN line breaks\r\n\n";
            string expected = "trailing RNN line breaks\r\n";


            //**  exercising the code  **//
            string actual = (string)pt.InvokeStatic("RemoveTrailingLineBreak", text);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveTrailingLineBreak__Ends_With_NNN__Assert_Only_Last_N_Removed()  /* working */  {
            //**  groundwork  **//
            Item item = new Item();
            PrivateType pt = new PrivateType(typeof(Item));

            string text = "trailing NNN line breaks\n\n\n";
            string expected = "trailing NNN line breaks\n\n";


            //**  exercising the code  **//
            string actual = (string)pt.InvokeStatic("RemoveTrailingLineBreak", text);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveTrailingLineBreak__Ends_With_RNRN__Assert_Only_Last_RN_Removed()  /* working */  {
            //**  groundwork  **//
            Item item = new Item();
            PrivateType pt = new PrivateType(typeof(Item));

            string text = "trailing RNRN line breaks\r\n\r\n";
            string expected = "trailing RNRN line breaks\r\n";


            //**  exercising the code  **//
            string actual = (string)pt.InvokeStatic("RemoveTrailingLineBreak", text);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveTrailingLineBreak__Ends_With_RN__Assert_Line_Break_Removed()  /* working */  {
            //**  groundwork  **//
            Item item = new Item();
            PrivateType pt = new PrivateType(typeof(Item));

            string text = "one trailing RN line break\r\n";
            string expected = "one trailing RN line break";


            //**  exercising the code  **//
            string actual = (string)pt.InvokeStatic("RemoveTrailingLineBreak", text);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RemoveTrailingLineBreak__Ends_With_N__Assert_Line_Break_Removed()  /* working */  {
            //**  groundwork  **//
            Item item = new Item();
            PrivateType pt = new PrivateType(typeof(Item));

            string text = "one trailing N line break\n";
            string expected = "one trailing N line break";


            //**  exercising the code  **//
            string actual = (string)pt.InvokeStatic("RemoveTrailingLineBreak", text);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion RemoveTrailingLineBreak()


        #region CombineWithLineBreak()

        [TestMethod()]
        public void CombineWithLineBreak__Knowns__Assert_Correct_Combined_Text()  /* working */  {
            //**  groundwork  **//
            Item item = new Item();
            PrivateObject p = new PrivateObject(item);

            string first = "first\r\n\n";
            string second = "second\n\n";


            string expected = "first\r\n\r\nsecond\n\n";


            //**  exercising the code  **//
            string actual = (string)p.Invoke("CombineWithLineBreak", first, second);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion CombineWithLineBreak()


        #region AddFromClipboardAtStart()

        [TestMethod()]
        public void AddFromClipboardAtStart_String_Arg__New_Has_Line_Break__Assert_Correct_Combined_Contents()  /* working */  {
            //**  groundwork  **//
            string original = "original text";
            Item item = new Item(original);

            string added = "new text\r\n";
            Clipboard.SetText(added);
            Thread.Sleep(1);

            string expected = added + original;


            //**  exercising the code  **//
            item.AddFromClipboardAtStart();


            //**  gathering results  **//
            string actual = item.Content;


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void AddFromClipboardAtStart_String_Arg__New_Lacks_Line_Break__Assert_Correct_Combined_Contents()  /* working */  {
            //**  groundwork  **//
            string original = "original text";
            Item item = new Item(original);

            string added = "new text";
            Clipboard.SetText(added);
            Thread.Sleep(1);

            string expected = added + Item.RETURN_NEW + original;


            //**  exercising the code  **//
            item.AddFromClipboardAtStart();


            //**  gathering results  **//
            string actual = item.Content;


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion AddFromClipboardAtStart()


        #region AddAtStart( string )

        [TestMethod()]
        public void AddAtStart_String_Arg__New_Has_Line_Break__Assert_Correct_Combined_Contents()  /* working */  {
            //**  groundwork  **//
            string original = "original text";
            Item item = new Item(original);

            string added = "new text\r\n";

            string expected = added + original;


            //**  exercising the code  **//
            item.AddAtStart(added);


            //**  gathering results  **//
            string actual = item.Content;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void AddAtStart_String_Arg__New_Lacks_Line_Break__Assert_Correct_Combined_Contents()  /* working */  {
            //**  groundwork  **//
            string original = "original text";
            Item item = new Item(original);

            string added = "new text";

            string expected = added + Item.RETURN_NEW + original;


            //**  exercising the code  **//
            item.AddAtStart(added);


            //**  gathering results  **//
            string actual = item.Content;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion AddAtStart( string )


        #region AddAtStart( List<Item> )

        [TestMethod()]
        public void AddAtStart_List_Of_Item_Args__Mixed_Ending_Types__Assert_Correct_Combined_Contents()  /* working */  {
            //**  groundwork  **//
            Item original = new Item("original\r\n");

            Item first = new Item("first\n");
            Item second = new Item("second");
            Item third = new Item("third\r\n\r\n\r\n");
            Item fourth = new Item("fourth");
            Item fifth = new Item("\nfifth\n\n");
            Item sixth = new Item("sixth\r\n\n");

            List<Item> items = new List<Item> { first, second, third, fourth, fifth, sixth };

            string expected = "first\r\nsecond\r\nthird\r\n\r\n\r\nfourth\r\n\nfifth\n\r\nsixth\r\n\r\noriginal\r\n";


            //**  exercising the code  **//
            original.AddAtStart(items);


            //**  gathering results  **//
            string actual = original.Content;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion AddAtStart( List<Item> )


        #region AddFromClipboardAtEnd()

        [TestMethod()]
        public void AddFromClipboardAtEnd_String_Arg__Original_Has_Line_Break__Assert_Correct_Combined_Contents()  /* working */  {
            //**  groundwork  **//
            string original = "original text\r\n";
            Item item = new Item(original);

            string added = "new text";
            Clipboard.SetText(added);
            Thread.Sleep(1);

            string expected = original + added;


            //**  exercising the code  **//
            item.AddFromClipboardAtEnd();


            //**  gathering results  **//
            string actual = item.Content;


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void AddFromClipboardAtEnd_String_Arg__Original_Lacks_Line_Break__Assert_Correct_Combined_Contents()  /* working */  {
            //**  groundwork  **//
            string original = "original text";
            Item item = new Item(original);

            string added = "new text";
            Clipboard.SetText(added);
            Thread.Sleep(1);

            string expected = original + Item.RETURN_NEW + added;


            //**  exercising the code  **//
            item.AddFromClipboardAtEnd();


            //**  gathering results  **//
            string actual = item.Content;


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion AddFromClipboardAtEnd()


        #region AddAtEnd( string )

        [TestMethod()]
        public void AddAtEnd_String_Arg__Original_Has_Line_Break__Assert_Correct_Combined_Contents()  /* working */  {
            //**  groundwork  **//
            string original = "original text\r\n";
            Item item = new Item(original);

            string added = "new text";

            string expected = original + added;


            //**  exercising the code  **//
            item.AddAtEnd(added);


            //**  gathering results  **//
            string actual = item.Content;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void AddAtEnd_String_Arg__Original_Lacks_Line_Break__Assert_Correct_Combined_Contents()  /* working */  {
            //**  groundwork  **//
            string original = "original text";
            Item item = new Item(original);

            string added = "new text";

            string expected = original + Item.RETURN_NEW + added;


            //**  exercising the code  **//
            item.AddAtEnd(added);


            //**  gathering results  **//
            string actual = item.Content;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion AddAtEnd( string )


        #region AddAtEnd( List<Item> )

        [TestMethod()]
        public void AddAtEnd_List_Of_Item_Args__Mixed_Ending_Types__Assert_Correct_Combined_Contents()  /* working */  {
            //**  groundwork  **//
            Item original = new Item("original");

            Item first = new Item("first\n");
            Item second = new Item("second");
            Item third = new Item("third\r\n\r\n\r\n");
            Item fourth = new Item("fourth");
            Item fifth = new Item("\nfifth\n\n");
            Item sixth = new Item("sixth\r\n\n");

            List<Item> items = new List<Item> { first, second, third, fourth, fifth, sixth };

            string expected = "original\r\nfirst\r\nsecond\r\nthird\r\n\r\n\r\nfourth\r\n\nfifth\n\r\nsixth\r\n\n";


            //**  exercising the code  **//
            original.AddAtEnd(items);


            //**  gathering results  **//
            string actual = original.Content;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion AddAtEnd( List<Item> )

    }
}