﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.ObjectModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using System.Windows;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Threading;
using QuickBoard;

namespace QuickBoardTests
{
    [TestClass]
    public class BoardTests
    {
        #region Fixtures

        private static Board FiveItemBoard()  /* verified */  {
            Board board = new Board();

            board.Add(new Item("first"));
            board.Add(new Item("second"));
            board.Add(new Item("third"));
            board.Add(new Item("fourth"));
            board.Add(new Item("fifth"));

            return (board);
        }

        private Board FiveItemBoardWithInsertedTopic(string topic, int insertAt)  /* passed */  {
            Board board = new Board();

            // fix'd items 
            Item one = new Item("one");
            Item two = new Item("two");
            Item three = new Item("three");
            Item four = new Item("four");

            board.Add(one);
            board.Add(two);
            board.Add(three);
            board.Add(four);

            // ensuring a usable @insertAt 
            insertAt = Math.Min(board.Count, insertAt);
            insertAt = Math.Max(insertAt, 0);

            // the @topic Item 
            board.Insert(insertAt, new Item(topic));

            // output 
            return (board);
        }

        private void SetClipboardToEmptyArrayAndText()  /* passed */  {
            // empty array 
            List<Item> empty = new List<Item>();

            // the two kinds of data to set on clipboard 
            string text = string.Empty;
            string serialized = Board.SerializeItems(empty);

            // placing both in a DataObject 
            DataObject toClipboard = new DataObject();
            toClipboard.SetText(text);
            toClipboard.SetData(ClipboardFormats.QuickBoardItemArray, serialized);

            // setting clipboard to DataObject 
            Clipboard.SetDataObject(toClipboard, true);
        }

        #endregion Fixtures


        #region Tests of fixtures

        #region Fixture FiveItemBoardWithInsertedTopic()

        [TestMethod()]
        public void FiveItemBoardWithInsertedTopic__Insert_At_0__Assert_Correct_Board_Contents()  /* working */  {
            //**  groundwork  **//
            string topic = "inserted";
            int at = 0;

            string[] expecteds = { "inserted", "one", "two", "three", "four" };


            //**  exercising the code  **//
            Board actual = FiveItemBoardWithInsertedTopic(topic, at);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actual.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], actual[i].Content);
            }
        }

        [TestMethod()]
        public void FiveItemBoardWithInsertedTopic__Insert_At_3__Assert_Correct_Board_Contents()  /* working */  {
            //**  groundwork  **//
            string topic = "inserted";
            int at = 3;

            string[] expecteds = { "one", "two", "three", "inserted", "four" };


            //**  exercising the code  **//
            Board actual = FiveItemBoardWithInsertedTopic(topic, at);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actual.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], actual[i].Content);
            }
        }

        [TestMethod()]
        public void FiveItemBoardWithInsertedTopic__Insert_At_5__Assert_Correct_Board_Contents()  /* working */  {
            //**  groundwork  **//
            string topic = "inserted";
            int at = 5;

            string[] expecteds = { "one", "two", "three", "four", "inserted" };


            //**  exercising the code  **//
            Board actual = FiveItemBoardWithInsertedTopic(topic, at);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actual.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], actual[i].Content);
            }
        }

        [TestMethod()]
        public void FiveItemBoardWithInsertedTopic__Insert_Below_0__Assert_Is_Inserted_At_0()  /* working */  {
            //**  groundwork  **//
            string topic = "inserted";
            int at = -6;

            string[] expecteds = { "inserted", "one", "two", "three", "four" };


            //**  exercising the code  **//
            Board actual = FiveItemBoardWithInsertedTopic(topic, at);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actual.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], actual[i].Content);
            }
        }

        [TestMethod()]
        public void FiveItemBoardWithInsertedTopic__Insert_Above_Top__Assert_Correct_Board_Contents()  /* working */  {
            //**  groundwork  **//
            string topic = "inserted";
            int at = 25;

            string[] expecteds = { "one", "two", "three", "four", "inserted" };


            //**  exercising the code  **//
            Board actual = FiveItemBoardWithInsertedTopic(topic, at);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, actual.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], actual[i].Content);
            }
        }

        #endregion Fixture FiveItemBoardWithInsertedTopic()


        #region Fixture SetClipboardToEmptyArrayAndText()

        [TestMethod()]
        public void SetClipboardToEmptyArrayAndText__No_Conditions__Assert_Clipboard_Custom_Data_Is_Empty_Array()  /* working */  {
            //**  groundwork  **//
            Thread.Sleep(1);

            List<Item> expected = new List<Item>();


            //**  exercising the code  **//
            SetClipboardToEmptyArrayAndText();


            //**  gathering results  **//
            string itemArrayFormat = ClipboardFormats.QuickBoardItemArray;
            string clipboardSerial = (string)Clipboard.GetData(itemArrayFormat);
            List<Item> actual = Board.ItemsFromSerial(clipboardSerial);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SetClipboardToEmptyArrayAndText__No_Conditions__Assert_Clipboard_Text_Is_Empty()  /* working */  {
            //**  groundwork  **//
            Thread.Sleep(1);

            string expected = string.Empty;

            //**  exercising the code  **//
            SetClipboardToEmptyArrayAndText();


            //**  gathering results  **//
            string actual = Clipboard.GetText();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion Fixture SetClipboardToEmptyArrayAndText()

        #endregion Fixture FiveItemBoardWithInsertedTopic()


        #region CanSave()

        [TestMethod()]
        public void CanSave__No_Items_No_File__Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            // inited with no Items and no file name 
            Board board = new Board();


            //**  exercising the code  **//
            bool actual = board.CanSave;


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void CanSave__Has_Items_No_File__Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            // inited with no Items and no file name 
            Board board = new Board();
            board.Add(new Item());
            board.Add(new Item());


            //**  exercising the code  **//
            bool actual = board.CanSave;


            //**  testing  **//
            Assert.AreEqual(false, actual);
        }

        [TestMethod()]
        public void CanSave__No_Items_Has_File__Assert_Returns_True()  /* working */  {
            //**  groundwork  **//
            // inited with no Items and no file name 
            Board board = new Board();
            board.SiteOnDisk = @"C:\Fake\Path\Mock.board";  // for now at least, any string is enough 


            //**  exercising the code  **//
            bool actual = board.CanSave;


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        [TestMethod()]
        public void CanSave__Has_Items_And_File__Assert_Returns_True()  /* working */  {
            //**  groundwork  **//
            Board board = new Board();
            board.Add(new Item());
            board.Add(new Item());

            board.SiteOnDisk = @"C:\Fake\Path\Mock.board";  // for now at least, any string is enough 


            //**  exercising the code  **//
            bool actual = board.CanSave;


            //**  testing  **//
            Assert.AreEqual(true, actual);
        }

        #endregion CanSave()


        #region CopyAllItemsToClipboard()

        [TestMethod()]
        public void CopyAllItemsToClipboard__Knowns__Assert_Correct_Clipboard_Contents()  /* working */  {
            //**  groundwork  **//
            Board board = new Board();

            Clipboard.SetText("test failed");  // if copying does not work, unchanged 
            Thread.Sleep(1);

            Item first = new Item("first\n");
            Item second = new Item("second");
            Item third = new Item("third\r\n\r\n\r\n");
            Item fourth = new Item("fourth");
            Item fifth = new Item("\nfifth\n\n");
            Item sixth = new Item("sixth\r\n\n");

            List<Item> items = new List<Item> { first, second, third, fourth, fifth, sixth };

            foreach (Item item in items) {
                board.Add(item);
            }

            string expected = "first\r\nsecond\r\nthird\r\n\r\n\r\nfourth\r\n\nfifth\n\r\nsixth\r\n\n";


            //**  exercising the code  **//
            board.CopyAllItemsToClipboard();


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetText();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CopyAllItemsToClipboard__Known_Items__Assert_All_Items_In_Custom_Data_Xml()  /* working */  {
            //**  groundwork  **//
            string expected = "unchanged";
            Clipboard.SetText(expected);
            Thread.Sleep(1);

            Board board = FiveItemBoard();

            // all should be present 
            string expAt0 = $"<Content>{ board[0].Content }</Content>";
            string expAt1 = $"<Content>{ board[1].Content }</Content>";
            string expAt2 = $"<Content>{ board[2].Content }</Content>";
            string expAt3 = $"<Content>{ board[3].Content }</Content>";
            string expAt4 = $"<Content>{ board[4].Content }</Content>";


            //**  exercising the code  **//
            board.CopyAllItemsToClipboard();


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = (string)Clipboard.GetData(ClipboardFormats.QuickBoardItemArray);


            //**  testing  **//
            StringAssert.Contains(actual, expAt0);
            StringAssert.Contains(actual, expAt1);
            StringAssert.Contains(actual, expAt2);
            StringAssert.Contains(actual, expAt3);
            StringAssert.Contains(actual, expAt4);
        }

        [TestMethod()]
        public void CopyAllItemsToClipboard__No_Items__Assert_No_Custom_Data_Xml()  /* working */  {
            /* when no Items on Board, nothing to save as text, 
             * so the empty List<Item> is not serialized either */

            //**  groundwork  **//
            string expected = "unchanged";
            Clipboard.SetText(expected);
            Thread.Sleep(1);

            // no Items, so nothing to serialize 
            Board board = new Board();


            //**  exercising the code  **//
            board.CopyAllItemsToClipboard();


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = (string)Clipboard.GetData(ClipboardFormats.QuickBoardItemArray);


            //**  testing  **//
            Assert.AreEqual(null, actual);
        }

        #endregion CopyAllItemsToClipboard()


        #region ClearAllWasCopiedFlags()

        [TestMethod()]
        public void ClearAllWasCopiedFlags__Some_Flags_Set__Assert_All_Flags_Cleared()  /* working */  {
            //**  groundwork  **//
            Item flagged1 = new Item("one");
            flagged1.WasCopied = true;

            Item unflagged1 = new Item("one");
            unflagged1.WasCopied = true;

            Item flagged2 = new Item("one");
            flagged2.WasCopied = true;

            Item unflagged2 = new Item("one");
            unflagged2.WasCopied = true;

            Board board = new Board { flagged1, unflagged1, flagged2, unflagged2 };


            //**  exercising the code  **//
            board.ClearAllWasCopiedFlags();


            //**  testing  **//
            Assert.AreEqual(false, flagged1.WasCopied);
            Assert.AreEqual(false, unflagged1.WasCopied);
            Assert.AreEqual(false, flagged2.WasCopied);
            Assert.AreEqual(false, unflagged2.WasCopied);
        }

        #endregion ClearAllWasCopiedFlags()


        #region SplitItem()

        [TestMethod()]
        public void SplitItem__Not_Splittable_At_0__Assert_Same_Board_Items()  /* working */  {
            //**  groundwork  **//
            string text = "this text is not splittable at hard returns";
            Board board = FiveItemBoardWithInsertedTopic(text, 0);
            Item item = board[0];

            string[] expecteds = { text, "one", "two", "three", "four" };


            //**  exercising the code  **//
            board.SplitItem(item);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void SplitItem__Splittable_At_0__Assert_Correct_Expanded_Board_Items()  /* working */  {
            //**  groundwork  **//
            string text = "this text is\r\nsplittable\r\nat hard returns";
            Board board = FiveItemBoardWithInsertedTopic(text, 0);
            Item item = board[0];

            string[] expecteds = { "this text is", "splittable", "at hard returns", "one", "two", "three", "four" };


            //**  exercising the code  **//
            board.SplitItem(item);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void SplitItem__Not_Splittable_In_Middle__Assert_Same_Board_Items()  /* working */  {
            //**  groundwork  **//
            string text = "this text is not splittable at hard returns";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            Item item = board[2];

            string[] expecteds = { "one", "two", text, "three", "four" };


            //**  exercising the code  **//
            board.SplitItem(item);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void SplitItem__Splittable_In_Middle__Assert_Correct_Expanded_Board_Items()  /* working */  {
            //**  groundwork  **//
            string text = "this text is\r\nsplittable\r\nat hard returns";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            Item item = board[2];

            string[] expecteds = { "one", "two", "this text is", "splittable", "at hard returns", "three", "four" };


            //**  exercising the code  **//
            board.SplitItem(item);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void SplitItem__Not_Splittable_At_Last__Assert_Same_Board_Items()  /* working */  {
            //**  groundwork  **//
            string text = "this text is not splittable at hard returns";
            Board board = FiveItemBoardWithInsertedTopic(text, 4);
            Item item = board[4];

            string[] expecteds = { "one", "two", "three", "four", text };


            //**  exercising the code  **//
            board.SplitItem(item);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void SplitItem__Splittable_At_Last__Assert_Correct_Expanded_Board_Items()  /* working */  {
            //**  groundwork  **//
            string text = "this text is\r\nsplittable\r\nat hard returns";
            Board board = FiveItemBoardWithInsertedTopic(text, 4);
            Item item = board[4];

            string[] expecteds = { "one", "two", "three", "four", "this text is", "splittable", "at hard returns" };


            //**  exercising the code  **//
            board.SplitItem(item);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        #endregion SplitItem()


        #region SiteToName()

        [TestMethod()]
        public void SiteToName__Realistic_Path__Assert_Returns_Correct_Name()  /* working */  {
            //**  groundwork  **//
            Board board = new Board();
            PrivateObject p = new PrivateObject(board);
            string arg = @"C:\Realistic\Location\On\Disk\ExpectedName.board";

            string expected = "ExpectedName";


            //**  exercising the code  **//
            string actual = (string)p.Invoke("SiteToName", arg);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SiteToName__Null_Arg__Assert_Returns_Empty_String()  /* working */  {
            //**  groundwork  **//
            Board board = new Board();
            PrivateObject p = new PrivateObject(board);
            string arg = null;

            string expected = string.Empty;


            //**  exercising the code  **//
            string actual = (string)p.Invoke("SiteToName", arg);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SiteToName__Empty_Arg__Assert_Returns_Empty_String()  /* working */  {
            //**  groundwork  **//
            Board board = new Board();
            PrivateObject p = new PrivateObject(board);
            string arg = string.Empty;

            string expected = string.Empty;


            //**  exercising the code  **//
            string actual = (string)p.Invoke("SiteToName", arg);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SiteToName__Nonstandard_Arg__Assert_Returns_Unaltered_Arg()  /* working */  {
            //**  groundwork  **//
            Board board = new Board();
            PrivateObject p = new PrivateObject(board);
            string arg = "Word";

            string expected = "Word";


            //**  exercising the code  **//
            string actual = (string)p.Invoke("SiteToName", arg);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion SiteToName()


        #region AddToGrouped( Item )

        [TestMethod()]
        public void AddToGrouped_Item__Known__Assert_Arg_In_Grouped()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoardWithInsertedTopic("grouped", 3);
            PrivateObject p = new PrivateObject(board);
            Item item = board[3];


            //**  exercising the code  **//
            board.AddToGrouped(item);


            //**  gathering results  **//
            List<Item> grouped = (List<Item>)p.GetProperty("Grouped");
            Item actual = grouped[0];


            //**  testing  **//
            Assert.AreEqual(1, board.CountOfGrouped);
            Assert.AreEqual(item, actual);
        }

        [TestMethod()]
        public void AddToGrouped_Item__Known__Assert_Arg_IsGrouped_True()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoardWithInsertedTopic("grouped", 3);
            Item item = board[3];
            item.IsGrouped = false;


            //**  exercising the code  **//
            board.AddToGrouped(item);


            //**  testing  **//
            Assert.AreEqual(true, item.IsGrouped);
        }

        [TestMethod()]
        public void AddToGrouped_Item__Added_Twice__Assert_Arg_In_Grouped_Once()  /* working */  {
            /* for reliability and predictability, no Item should be in .Grouped twice */

            //**  groundwork  **//
            Board board = FiveItemBoardWithInsertedTopic("grouped", 3);
            PrivateObject p = new PrivateObject(board);
            Item item = board[3];


            //**  exercising the code  **//
            board.AddToGrouped(item);
            board.AddToGrouped(item);


            //**  gathering results  **//
            List<Item> grouped = (List<Item>)p.GetProperty("Grouped");

            int actual = grouped
                .Count(x => (x == item));


            //**  testing  **//
            Assert.AreEqual(1, board.CountOfGrouped);
            Assert.AreEqual(1, actual);
        }

        #endregion AddToGrouped( Item )


        #region AddToGrouped( params Item[] )

        [TestMethod()]
        public void AddToGrouped_Params__Knowns__Assert_Args_In_Grouped()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoardWithInsertedTopic("grouped", 3);
            PrivateObject p = new PrivateObject(board);

            Item itemAt1 = board[1];
            Item itemAt3 = board[3];

            Item[] expecteds = { itemAt1, itemAt3 };


            //**  exercising the code  **//
            board.AddToGrouped(itemAt1, itemAt3);


            //**  gathering results  **//
            List<Item> actuals = (List<Item>)p.GetProperty("Grouped");


            //**  testing  **//
            Assert.AreEqual(2, board.CountOfGrouped);
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        [TestMethod()]
        public void AddToGrouped_Params__Known__Assert_Args_IsGrouped_True()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoardWithInsertedTopic("grouped", 3);
            Item itemAt1 = board[1];
            Item itemAt3 = board[3];
            itemAt1.IsGrouped = false;
            itemAt3.IsGrouped = false;


            //**  exercising the code  **//
            board.AddToGrouped(itemAt1, itemAt3);


            //**  testing  **//
            Assert.AreEqual(true, itemAt1.IsGrouped);
            Assert.AreEqual(true, itemAt1.IsGrouped);
        }

        #endregion AddToGrouped( params Item[] )


        #region RemoveFromGrouped( Item )

        [TestMethod()]
        public void RemoveFromGrouped_Item__Knowns__Assert_Arg_Not_In_Grouped()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoardWithInsertedTopic("grouped", 3);
            PrivateObject p = new PrivateObject(board);

            Item itemAt0 = board[0];
            Item itemAt2 = board[2];
            Item itemAt3 = board[3];
            board.AddToGrouped(itemAt0, itemAt2, itemAt3);

            Item[] expecteds = { itemAt0, itemAt3 };


            //**  exercising the code  **//
            board.RemoveFromGrouped(itemAt2);


            //**  gathering results  **//
            List<Item> actuals = (List<Item>)p.GetProperty("Grouped");


            //**  testing  **//
            Assert.AreEqual(2, board.CountOfGrouped);
            CollectionAssert.AreEqual(expecteds, actuals);
        }

        [TestMethod()]
        public void RemoveFromGrouped_Item__Knowns__Assert_Arg_IsGrouped_False()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoardWithInsertedTopic("grouped", 3);
            PrivateObject p = new PrivateObject(board);

            Item itemAt0 = board[0];
            Item itemAt2 = board[2];
            Item itemAt3 = board[3];
            board.AddToGrouped(itemAt0, itemAt2, itemAt3);


            //**  exercising the code  **//
            board.RemoveFromGrouped(itemAt2);


            //**  gathering results  **//
            List<Item> actuals = (List<Item>)p.GetProperty("Grouped");


            //**  testing  **//
            Assert.AreEqual(false, itemAt2.IsGrouped);
        }

        #endregion RemoveFromGrouped( Item )


        #region RemoveFromGrouped( params )

        [TestMethod()]
        public void RemoveFromGrouped_Params__Knowns__Assert_Args_Not_In_Grouped()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoardWithInsertedTopic("grouped", 3);
            PrivateObject p = new PrivateObject(board);

            Item itemAt0 = board[0];
            Item itemAt2 = board[2];
            Item itemAt3 = board[3];
            board.AddToGrouped(itemAt0, itemAt2, itemAt3);

            Item expected = itemAt2;


            //**  exercising the code  **//
            board.RemoveFromGrouped(itemAt0, itemAt3);


            //**  gathering results  **//
            List<Item> actuals = (List<Item>)p.GetProperty("Grouped");


            //**  testing  **//
            Assert.AreEqual(1, board.CountOfGrouped);
            Assert.AreEqual(expected, actuals[0]);
        }

        [TestMethod()]
        public void RemoveFromGrouped_Params__Knowns__Assert_Args_IsGrouped_False()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoardWithInsertedTopic("grouped", 3);
            PrivateObject p = new PrivateObject(board);

            Item itemAt0 = board[0];
            Item itemAt2 = board[2];
            Item itemAt3 = board[3];
            board.AddToGrouped(itemAt0, itemAt2, itemAt3);


            //**  exercising the code  **//
            board.RemoveFromGrouped(itemAt0, itemAt3);


            //**  gathering results  **//
            List<Item> actuals = (List<Item>)p.GetProperty("Grouped");


            //**  testing  **//
            Assert.AreEqual(false, itemAt0.IsGrouped);
            Assert.AreEqual(false, itemAt3.IsGrouped);
        }

        #endregion RemoveFromGrouped( params )


        #region ClearGrouped()

        [TestMethod()]
        public void ClearGrouped__Knowns__Assert_Grouped_Empty()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoardWithInsertedTopic("not header", 0);
            board.AddToGrouped(board[0], board[2], board[4]);


            //**  exercising the code  **//
            board.ClearGrouped();


            //**  testing  **//
            Assert.AreEqual(0, board.CountOfGrouped);
        }

        [TestMethod()]
        public void ClearGrouped__Knowns__Assert_Items_IsGrouped_False()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoardWithInsertedTopic("not header", 0);
            board.AddToGrouped(board[0], board[2], board[4]);

            // needed because .Grouped itself is cleared after Toggle__() 
            List<Item> items = new List<Item> { board[0], board[2], board[4] };


            //**  exercising the code  **//
            board.ClearGrouped();


            //**  testing  **//
            foreach (Item item in board) {
                Assert.AreEqual(false, item.IsGrouped);
            }
        }

        #endregion ClearGrouped()


        #region MergeGrouped()

        [TestMethod()]
        public void MergeGrouped__Single_At_0__Assert_Same_Board_Items()  /* working */  {
            //**  groundwork  **//
            string text = "not merged";
            Board board = FiveItemBoardWithInsertedTopic(text, 0);
            board.AddToGrouped(board[0]);

            // before change, same as @expecteds 

            string[] expecteds = { text, "one", "two", "three", "four" };


            //**  exercising the code  **//
            board.MergeGrouped();


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void MergeGrouped__Several_From_0__Assert_Correct_Reduced_Board_Items()  /* working */  {
            //**  groundwork  **//
            string text = "start of merge";
            Board board = FiveItemBoardWithInsertedTopic(text, 0);
            board.AddToGrouped(board[0], board[2], board[3]);  // noncontiguous 

            // before change: { text, "one", "two", "three", "four" } 

            string[] expecteds = { $"{ text }\r\ntwo\r\nthree", "one", "four" };


            //**  exercising the code  **//
            board.MergeGrouped();


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void MergeGrouped__Single_In_Middle__Assert_Same_Board_Items()  /* working */  {
            //**  groundwork  **//
            string text = "not merged";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[1]);  // Item retaining "two" 

            // before change, same as @expecteds 

            string[] expecteds = { "one", "two", text, "three", "four" };


            //**  exercising the code  **//
            board.MergeGrouped();


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void MergeGrouped__Several_In_Middle__Assert_Correct_Reduced_Board_Items()  /* working */  {
            //**  groundwork  **//
            string text = "start of merge";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            // before change: { "one", "two", text, "three", "four" } 

            string[] expecteds = { "one", $"{ text }\r\ntwo", "three", "four" };


            //**  exercising the code  **//
            board.MergeGrouped();


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void MergeGrouped__Several_In_Middle_And_At_0__Assert_Correct_Reduced_Board_Items()  /* working */  {
            //**  groundwork  **//
            string text = "end of merge";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[3], board[0], board[2]);  // @2 is Item retaining @text 

            // before change: { "one", "two", text, "three", "four" } 

            string[] expecteds = { "two", "three\r\none\r\nend of merge", "four" };


            //**  exercising the code  **//
            board.MergeGrouped();


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void MergeGrouped__Several_In_Middle_And_At_Last__Assert_Correct_Reduced_Board_Items()  /* working */  {
            //**  groundwork  **//
            string text = "middle of merge";
            Board board = FiveItemBoardWithInsertedTopic(text, 4);  // 4 is end 
            board.AddToGrouped(board[1], board[4], board[3]);

            // before change: { "one", "two", "three", "four", text } 

            string[] expecteds = { "one", $"two\r\n{ text }\r\nfour", "three" };


            //**  exercising the code  **//
            board.MergeGrouped();


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void MergeGrouped__Single_At_Last__Assert_Same_Board_Items()  /* working */  {
            //**  groundwork  **//
            string text = "not merged";
            Board board = FiveItemBoardWithInsertedTopic(text, 4);  // 4 is end 
            board.AddToGrouped(board[4]);

            // before change, same as @expecteds 

            string[] expecteds = { "one", "two", "three", "four", text };


            //**  exercising the code  **//
            board.MergeGrouped();


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void MergeGrouped__Several_At_Last__Assert_Correct_Reduced_Board_Items()  /* working */  {
            //**  groundwork  **//
            string text = "not merged";
            Board board = FiveItemBoardWithInsertedTopic(text, 4);  // 4 is end 
            board.AddToGrouped(board[4], board[0], board[2]);

            // before: { 2-"one", "two", 3-"three", "four", 1-text } 

            string[] expecteds = { "two", "four", $"{ text }\r\none\r\nthree" };


            //**  exercising the code  **//
            board.MergeGrouped();


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void MergeGrouped__Some_Items__Assert_Correct_Board_Contents()  /* working */  {
            //**  groundwork  **//
            string text = "start of merge";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            // before change: { "one", "two", text, "three", "four" } 

            string[] expecteds = { "one", $"{ text }\r\ntwo", "three", "four" };


            //**  exercising the code  **//
            board.MergeGrouped();


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void MergeGrouped__Three_Items__Assert_Correct_Board_Contents()  /* working */  {
            /* with 3 items, an incorrect removal loop will leave the last item unremoved */

            //**  groundwork  **//
            string text = "start of merge";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1], board[4]);

            // before change: { "one", "two", text, "three", "four" } 

            string[] expecteds = { "one", $"{ text }\r\ntwo\r\nfour", "three" };


            //**  exercising the code  **//
            board.MergeGrouped();


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void MergeGrouped__Some_Items__Assert_Grouped_Empty()  /* working */  {
            //**  groundwork  **//
            string text = "start of merge";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            // before change: { "one", "two", text, "three", "four" } 

            string[] expecteds = { "one", $"{ text }\r\ntwo", "three", "four" };


            //**  exercising the code  **//
            board.MergeGrouped();


            //**  testing  **//
            Assert.AreEqual(0, board.CountOfGrouped);
        }

        #endregion MergeGrouped()


        #region ToggleHeaderOnGrouped()

        [TestMethod()]
        public void ToggleHeaderOnGrouped__First_Item_Not_Header__Assert_All_IsHeader_True()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoardWithInsertedTopic("not header", 0);
            board.AddToGrouped(board[0], board[2], board[4]);

            // needed because .Grouped itself is cleared after Toggle__() 
            List<Item> items = new List<Item> { board[0], board[2], board[4] };

            // setting mixed state 
            board[0].IsHeader = false;
            board[1].IsHeader = true;
            board[2].IsHeader = false;


            //**  exercising the code  **//
            board.ToggleHeaderOnGrouped();


            //**  testing  **//
            foreach (Item item in items) {
                Assert.AreEqual(true, item.IsHeader);
            }
        }

        [TestMethod()]
        public void ToggleHeaderOnGrouped__First_Item_Header__Assert_All_IsHeader_False()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoardWithInsertedTopic("header", 0);
            board.AddToGrouped(board[0], board[2], board[4]);

            // needed because .Grouped itself is cleared after Toggle__() 
            List<Item> items = new List<Item> { board[0], board[2], board[4] };

            // setting mixed state 
            items[0].IsHeader = true;
            items[1].IsHeader = false;
            items[2].IsHeader = true;


            //**  exercising the code  **//
            board.ToggleHeaderOnGrouped();


            //**  testing  **//
            foreach (Item item in items) {
                Assert.AreEqual(false, item.IsHeader);
            }
        }

        [TestMethod()]
        public void ToggleHeaderOnGrouped__Knowns__Assert_Grouped_Unchanged()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoardWithInsertedTopic("not header", 0);
            board.AddToGrouped(board[0], board[2], board[4]);

            // needed because .Grouped itself is cleared after Toggle__() 
            List<Item> items = new List<Item> { board[0], board[2], board[4] };

            // setting mixed state 
            board[0].IsHeader = false;
            board[1].IsHeader = true;
            board[2].IsHeader = false;


            //**  exercising the code  **//
            board.ToggleHeaderOnGrouped();


            //**  testing  **//
            Assert.AreEqual(3, board.CountOfGrouped);
        }

        #endregion ToggleHeaderOnGrouped()


        #region ClearWasCopiedOnGrouped()

        [TestMethod()]
        public void ClearWasCopiedOnGrouped__Mixed_WasCopied__Assert_All_WasCopied_False()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoardWithInsertedTopic("header", 0);
            board.AddToGrouped(board[0], board[2], board[4]);

            // needed because .Grouped itself is cleared after Toggle__() 
            List<Item> items = new List<Item> { board[0], board[2], board[4] };

            // setting mixed state 
            items[0].WasCopied = true;
            items[1].WasCopied = false;
            items[2].WasCopied = true;


            //**  exercising the code  **//
            board.ClearWasCopiedOnGrouped();


            //**  testing  **//
            foreach (Item item in items) {
                Assert.AreEqual(false, item.WasCopied);
            }
        }

        [TestMethod()]
        public void ClearWasCopiedOnGrouped__Knowns__Assert_Grouped_Unchanged()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoardWithInsertedTopic("not header", 0);
            board.AddToGrouped(board[0], board[2], board[4]);

            // needed because .Grouped itself is cleared after Toggle__() 
            List<Item> items = new List<Item> { board[0], board[2], board[4] };

            // setting mixed state 
            board[0].WasCopied = false;
            board[1].WasCopied = true;
            board[2].WasCopied = false;


            //**  exercising the code  **//
            board.ClearWasCopiedOnGrouped();


            //**  testing  **//
            Assert.AreEqual(3, board.CountOfGrouped);
        }

        #endregion ClearWasCopiedOnGrouped()


        #region CopyGroupedToClipboard()

        [TestMethod()]
        public void CopyGroupedToClipboard__Known_Items__Assert_Merged_Text_On_Clipboard()  /* working */  {
            //**  groundwork  **//
            string text = "start of copy";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            Clipboard.SetText("original text");
            Thread.Sleep(1);

            string expected = $"{ text }\r\ntwo";


            //**  exercising the code  **//
            board.CopyGroupedToClipboard();


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetText();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CopyGroupedToClipboard__Known_Items__Assert_Grouped_Unchanged()  /* working */  {
            //**  groundwork  **//
            string text = "start of copy";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            Thread.Sleep(1);

            // before change: { "one", "two", text, "three", "four" } 

            string[] expecteds = { "one", $"{ text }\r\ntwo", "three", "four" };


            //**  exercising the code  **//
            board.CopyGroupedToClipboard();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(2, board.CountOfGrouped);
        }

        [TestMethod()]
        public void CopyGroupedToClipboard__Known_Items__Assert_Grouped_Items_In_Custom_Data_Xml()  /* working */  {
            //**  groundwork  **//
            string expected = "unchanged";
            Clipboard.SetText(expected);
            Thread.Sleep(1);

            Board board = FiveItemBoard();
            board.AddToGrouped(board[1]);
            board.AddToGrouped(board[3]);

            // these should be present 
            string expAt1 = $"<Content>{ board[1].Content }</Content>";
            string expAt3 = $"<Content>{ board[3].Content }</Content>";


            //**  exercising the code  **//
            board.CopyGroupedToClipboard();


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = (string)Clipboard.GetData(ClipboardFormats.QuickBoardItemArray);


            //**  testing  **//
            StringAssert.Contains(actual, expAt1);
            StringAssert.Contains(actual, expAt3);
        }

        [TestMethod()]
        public void CopyGroupedToClipboard__Known_Items_Grouped__Assert_Non_Grouped_Items_Not_In_Custom_Data_Xml()  /* working */  {
            //**  groundwork  **//
            string expected = "unchanged";
            Clipboard.SetText(expected);
            Thread.Sleep(1);

            Board board = FiveItemBoard();
            board.AddToGrouped(board[1]);
            board.AddToGrouped(board[3]);

            // these should not be present 
            Regex expNotAt0 = new Regex($"<Content>{ board[0].Content }</Content>");
            Regex expNotAt2 = new Regex($"<Content>{ board[2].Content }</Content>");
            Regex expNotAt4 = new Regex($"<Content>{ board[4].Content }</Content>");


            //**  exercising the code  **//
            board.CopyGroupedToClipboard();


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = (string)Clipboard.GetData(ClipboardFormats.QuickBoardItemArray);


            //**  testing  **//
            StringAssert.DoesNotMatch(actual, expNotAt0);
            StringAssert.DoesNotMatch(actual, expNotAt2);
            StringAssert.DoesNotMatch(actual, expNotAt4);
        }

        [TestMethod()]
        public void CopyGroupedToClipboard__No_Items__Assert_No_Custom_Data_Xml()  /* working */  {
            /* when no Items in .Grouped, nothing to save as text, 
             * so the empty List<Item> is not serialized either */

            //**  groundwork  **//
            string expected = "unchanged";
            Clipboard.SetText(expected);
            Thread.Sleep(1);

            // no Items in .Grouped, so nothing to serialize 
            Board board = FiveItemBoard();


            //**  exercising the code  **//
            board.CopyGroupedToClipboard();


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = (string)Clipboard.GetData(ClipboardFormats.QuickBoardItemArray);


            //**  testing  **//
            Assert.AreEqual(null, actual);
        }

        #endregion CopyGroupedToClipboard()


        #region CopyGroupedToClipboardStart()

        [TestMethod()]
        public void CopyGroupedToClipboardStart__Some_Items__Assert_At_Clipboard_Start()  /* working */  {
            //**  groundwork  **//
            string text = "start of copy";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            Clipboard.SetText("original text");
            Thread.Sleep(1);

            string expected = $"{ text }\r\ntwo\r\noriginal text";


            //**  exercising the code  **//
            board.CopyGroupedToClipboardStart();


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetText();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CopyGroupedToClipboardStart__Some_Items__Assert_Grouped_Unchanged()  /* working */  {
            //**  groundwork  **//
            string text = "start of copy";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            Thread.Sleep(1);

            // before change: { "one", "two", text, "three", "four" } 

            string[] expecteds = { "one", $"{ text }\r\ntwo", "three", "four" };


            //**  exercising the code  **//
            board.CopyGroupedToClipboardStart();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(2, board.CountOfGrouped);
        }

        #endregion CopyGroupedToClipboardStart()


        #region CopyGroupedToClipboardEnd()

        [TestMethod()]
        public void CopyGroupedToClipboardEnd__Some_Items__Assert_At_Clipboard_End()  /* working */  {
            //**  groundwork  **//
            string text = "start of copy";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            Clipboard.SetText("original text");
            Thread.Sleep(1);

            string expected = $"original text\r\n{ text }\r\ntwo";


            //**  exercising the code  **//
            board.CopyGroupedToClipboardEnd();


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetText();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CopyGroupedToClipboardEnd__Some_Items__Assert_Grouped_Unchanged()  /* working */  {
            //**  groundwork  **//
            string text = "start of copy";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            Thread.Sleep(1);

            // before change: { "one", "two", text, "three", "four" } 

            string[] expecteds = { "one", $"{ text }\r\ntwo", "three", "four" };


            //**  exercising the code  **//
            board.CopyGroupedToClipboardEnd();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(2, board.CountOfGrouped);
        }

        #endregion CopyGroupedToClipboardEnd()


        #region CutGroupedToClipboard()

        [TestMethod()]
        public void CutGroupedToClipboard__Some_Items__Assert_Text_On_Clipboard()  /* working */  {
            //**  groundwork  **//
            string text = "start of cut";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            Clipboard.SetText("original text");
            Thread.Sleep(1);

            string expected = $"{ text }\r\ntwo";


            //**  exercising the code  **//
            board.CutGroupedToClipboard();


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetText();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CutGroupedToClipboard__Some_Items__Assert_Correct_Board_Contents()  /* working */  {
            /* when Items are cut to the Clipboard, they are no longer on the Board */

            //**  groundwork  **//
            string text = "start of cut";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            Thread.Sleep(1);

            // before change: { "one", "two", text, "three", "four" } 

            string[] expecteds = { "one", "three", "four" };


            //**  exercising the code  **//
            board.CutGroupedToClipboard();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void CutGroupedToClipboard__Some_Items__Assert_Grouped_Empty()  /* working */  {
            //**  groundwork  **//
            string text = "start of cut";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            Thread.Sleep(1);

            // before change: { "one", "two", text, "three", "four" } 

            string[] expecteds = { "one", $"{ text }\r\ntwo", "three", "four" };


            //**  exercising the code  **//
            board.CutGroupedToClipboard();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(0, board.CountOfGrouped);
        }

        [TestMethod()]
        public void CutGroupedToClipboard__Known_Items__Assert_Grouped_Items_In_Custom_Data_Xml()  /* working */  {
            //**  groundwork  **//
            string expected = "unchanged";
            Clipboard.SetText(expected);
            Thread.Sleep(1);

            Board board = FiveItemBoard();
            board.AddToGrouped(board[1]);
            board.AddToGrouped(board[3]);

            // these should be present 
            string expAt1 = $"<Content>{ board[1].Content }</Content>";
            string expAt3 = $"<Content>{ board[3].Content }</Content>";


            //**  exercising the code  **//
            board.CutGroupedToClipboard();


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = (string)Clipboard.GetData(ClipboardFormats.QuickBoardItemArray);


            //**  testing  **//
            StringAssert.Contains(actual, expAt1);
            StringAssert.Contains(actual, expAt3);
        }

        [TestMethod()]
        public void CutGroupedToClipboard__Known_Items_Grouped__Assert_Non_Grouped_Items_Not_In_Custom_Data_Xml()  /* working */  {
            //**  groundwork  **//
            string expected = "unchanged";
            Clipboard.SetText(expected);
            Thread.Sleep(1);

            Board board = FiveItemBoard();
            board.AddToGrouped(board[1]);
            board.AddToGrouped(board[3]);

            // these should not be present 
            Regex expNotAt0 = new Regex($"<Content>{ board[0].Content }</Content>");
            Regex expNotAt2 = new Regex($"<Content>{ board[2].Content }</Content>");
            Regex expNotAt4 = new Regex($"<Content>{ board[4].Content }</Content>");


            //**  exercising the code  **//
            board.CutGroupedToClipboard();


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = (string)Clipboard.GetData(ClipboardFormats.QuickBoardItemArray);


            //**  testing  **//
            StringAssert.DoesNotMatch(actual, expNotAt0);
            StringAssert.DoesNotMatch(actual, expNotAt2);
            StringAssert.DoesNotMatch(actual, expNotAt4);
        }

        [TestMethod()]
        public void CutGroupedToClipboard__No_Items__Assert_No_Custom_Data_Xml()  /* working */  {
            /* when no Items in .Grouped, nothing to save as text, 
             * so the empty List<Item> is not serialized either */

            //**  groundwork  **//
            string expected = "unchanged";
            Clipboard.SetText(expected);
            Thread.Sleep(1);

            // no Items in .Grouped, so nothing to serialize 
            Board board = FiveItemBoard();


            //**  exercising the code  **//
            board.CutGroupedToClipboard();


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = (string)Clipboard.GetData(ClipboardFormats.QuickBoardItemArray);


            //**  testing  **//
            Assert.AreEqual(null, actual);
        }

        #endregion CutGroupedToClipboard()


        #region CutGroupedToClipboardStart()

        [TestMethod()]
        public void CutGroupedToClipboardStart__Some_Items__Assert_At_Clipboard_Start()  /* working */  {
            //**  groundwork  **//
            string text = "start of cut";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            Clipboard.SetText("original text");
            Thread.Sleep(1);

            string expected = $"{ text }\r\ntwo\r\noriginal text";


            //**  exercising the code  **//
            board.CutGroupedToClipboardStart();


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetText();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CutGroupedToClipboardStart__Some_Items__Assert_Correct_Board_Contents()  /* working */  {
            //**  groundwork  **//
            string text = "start of cut";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            Thread.Sleep(1);

            // before change: { "one", "two", text, "three", "four" } 

            string[] expecteds = { "one", "three", "four" };


            //**  exercising the code  **//
            board.CutGroupedToClipboardStart();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void CutGroupedToClipboardStart__Some_Items__Assert_Grouped_Empty()  /* working */  {
            //**  groundwork  **//
            string text = "start of cut";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            Thread.Sleep(1);

            // before change: { "one", "two", text, "three", "four" } 

            string[] expecteds = { "one", $"{ text }\r\ntwo", "three", "four" };


            //**  exercising the code  **//
            board.CutGroupedToClipboardStart();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(0, board.CountOfGrouped);
        }

        #endregion CutGroupedToClipboardStart()


        #region CutGroupedToClipboardEnd()

        [TestMethod()]
        public void CutGroupedToClipboardEnd__Some_Items__Assert_At_Clipboard_End()  /* working */  {
            //**  groundwork  **//
            string text = "start of cut";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            Clipboard.SetText("original text");
            Thread.Sleep(1);

            string expected = $"original text\r\n{ text }\r\ntwo";


            //**  exercising the code  **//
            board.CutGroupedToClipboardEnd();


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetText();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CutGroupedToClipboardEnd__Some_Items__Assert_Correct_Board_Contents()  /* working */  {
            //**  groundwork  **//
            string text = "start of cut";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            Thread.Sleep(1);

            // before change: { "one", "two", text, "three", "four" } 

            string[] expecteds = { "one", "three", "four" };


            //**  exercising the code  **//
            board.CutGroupedToClipboardEnd();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void CutGroupedToClipboardEnd__Some_Items__Assert_Grouped_Empty()  /* working */  {
            //**  groundwork  **//
            string text = "start of cut";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            Thread.Sleep(1);

            // before change: { "one", "two", text, "three", "four" } 

            string[] expecteds = { "one", $"{ text }\r\ntwo", "three", "four" };


            //**  exercising the code  **//
            board.CutGroupedToClipboardEnd();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(0, board.CountOfGrouped);
        }

        #endregion CutGroupedToClipboardEnd()


        #region GroupedItem()

        [TestMethod()]
        public void GroupedItem__Arg_Below_Zero__Assert_Returns_Null() {
            //**  groundwork  **//
            Item first = new Item("first");
            Item second = new Item("second");
            Item third = new Item("third");
            List<Item> items = new List<Item> { first, second, third };

            Board board = new Board(items);
            board.AddToGrouped(board[1], board[2]);

            int arg = -2;


            //**  exercising the code  **//
            Item actual = board.GroupedItem(arg);


            //**  testing  **//
            Assert.AreEqual(null, actual);
        }

        [TestMethod()]
        public void GroupedItem__Arg_At_Count__Assert_Returns_Null() {
            //**  groundwork  **//
            Item first = new Item("first");
            Item second = new Item("second");
            Item third = new Item("third");
            List<Item> items = new List<Item> { first, second, third };

            Board board = new Board(items);
            board.AddToGrouped(board[1], board[2]);

            int arg = board.CountOfGrouped;


            //**  exercising the code  **//
            Item actual = board.GroupedItem(arg);


            //**  testing  **//
            Assert.AreEqual(null, actual);
        }

        [TestMethod()]
        public void GroupedItem__Arg_Above_Count__Assert_Returns_Null() {
            //**  groundwork  **//
            Item first = new Item("first");
            Item second = new Item("second");
            Item third = new Item("third");
            List<Item> items = new List<Item> { first, second, third };

            Board board = new Board(items);
            board.AddToGrouped(board[1], board[2]);

            int arg = board.CountOfGrouped + 3;


            //**  exercising the code  **//
            Item actual = board.GroupedItem(arg);


            //**  testing  **//
            Assert.AreEqual(null, actual);
        }

        [TestMethod()]
        public void GroupedItem__Valid_Arg__Assert_Returns_Correct_Item() {
            //**  groundwork  **//
            Item first = new Item("first");
            Item second = new Item("second");
            Item third = new Item("third");
            List<Item> items = new List<Item> { first, second, third };

            Board board = new Board(items);
            board.AddToGrouped(board[1], board[2]);

            int arg = 0;


            //**  exercising the code  **//
            Item actual = board.GroupedItem(arg);


            //**  testing  **//
            Assert.AreEqual(second, actual);  // @second is first in .Grouped 
        }

        #endregion GroupedItem()


        #region RemoveGrouped()

        [TestMethod()]
        public void RemoveGrouped__Some_Items__Assert_Correct_Board_Contents()  /* working */  {
            //**  groundwork  **//
            string text = "start of cut";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            // before change: { "one", "two", text, "three", "four" } 

            string[] expecteds = { "one", "three", "four" };


            //**  exercising the code  **//
            board.RemoveGrouped();


            //**  testing  **//
            Assert.AreEqual(expecteds.Length, board.Count);

            for (int i = 0; i < expecteds.Length; i++) {
                Assert.AreEqual(expecteds[i], board[i].Content);
            }
        }

        [TestMethod()]
        public void RemoveGrouped__Some_Items__Assert_Grouped_Empty()  /* working */  {
            //**  groundwork  **//
            string text = "start of remove";
            Board board = FiveItemBoardWithInsertedTopic(text, 2);
            board.AddToGrouped(board[2], board[1]);

            // before change: { "one", "two", text, "three", "four" } 

            string[] expecteds = { "one", $"{ text }\r\ntwo", "three", "four" };


            //**  exercising the code  **//
            board.RemoveGrouped();


            //**  testing  **//
            Assert.AreEqual(0, board.CountOfGrouped);
        }

        #endregion RemoveGrouped()


        #region Remove()

        [TestMethod()]
        public void Remove__Item_Is_In_Grouped__Assert_Item_Removed_From_Board_And_Grouped()  /* working */  {
            //**  groundwork  **//
            Item first = new Item("first");
            Item second = new Item("second");
            Item third = new Item("third");
            List<Item> items = new List<Item> { first, second, third };

            Board board = new Board(items);
            PrivateObject p = new PrivateObject(board);

            board.AddToGrouped(board[1], board[2]);


            //**  exercising the code  **//
            board.Remove(board[2]);


            //**  gathering results  **//
            List<Item> actuals = (List<Item>)p.GetProperty("Grouped");


            //**  testing  **//
            Assert.AreEqual(2, board.Count);
            Assert.AreEqual(board[0], first);
            Assert.AreEqual(board[1], second);

            Assert.AreEqual(1, board.CountOfGrouped);
            Assert.AreEqual(board[1], actuals[0]);
        }

        [TestMethod()]
        public void Remove__Item_Not_In_Grouped__Assert_Item_Removed_From_Board_But_Grouped_Unchanged()  /* working */  {
            //**  groundwork  **//
            Item first = new Item("first");
            Item second = new Item("second");
            Item third = new Item("third");
            List<Item> items = new List<Item> { first, second, third };

            Board board = new Board(items);
            PrivateObject p = new PrivateObject(board);

            board.AddToGrouped(board[0], board[2]);


            //**  exercising the code  **//
            board.Remove(board[1]);


            //**  gathering results  **//
            List<Item> actuals = (List<Item>)p.GetProperty("Grouped");


            //**  testing  **//
            Assert.AreEqual(2, board.Count);
            Assert.AreEqual(board[0], first);
            Assert.AreEqual(board[1], third);

            Assert.AreEqual(2, board.CountOfGrouped);
            Assert.AreEqual(board[0], actuals[0]);
            Assert.AreEqual(board[1], actuals[1]);  // after Remove(), @board's last Item is at 1 
        }

        #endregion Remove()


        #region .CanInsertItemsFromClipboard

        [TestMethod()]
        public void CanInsertItemsFromClipboard__Clipboard_Empty__Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            Clipboard.Clear();
            Thread.Sleep(1);

            bool expected = false;


            //**  exercising the code  **//
            bool actual = Board.CanInsertItemsFromClipboard;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CanInsertItemsFromClipboard__Text_On_Clipboard__Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            Clipboard.SetText("item text");
            Thread.Sleep(1);

            bool expected = false;


            //**  exercising the code  **//
            bool actual = Board.CanInsertItemsFromClipboard;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CanInsertItemsFromClipboard__Single_Item_On_Clipboard__Assert_Returns_False()  /* working */  {
            //**  groundwork  **//
            Item item = new Item("item content", true);
            item.ToClipboard();
            Thread.Sleep(1);

            bool expected = false;


            //**  exercising the code  **//
            bool actual = Board.CanInsertItemsFromClipboard;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CanInsertItemsFromClipboard__Items_Array_On_Clipboard__Assert_Returns_True()  /* working */  {
            //**  groundwork  **//
            Item first = new Item("one", true);
            Item second = new Item("two");

            List<Item> items = new List<Item> { first, second };
            Board.ItemsToClipboard(items);
            Thread.Sleep(1);

            bool expected = true;


            //**  exercising the code  **//
            bool actual = Board.CanInsertItemsFromClipboard;


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion .CanInsertItemsFromClipboard


        #region SerializeItems()

        [TestMethod()]
        public void SerializeItems__Known_Items__Assert_Correct_Values_In_Xml()  /* working */  {
            //**  groundwork  **//
            Item one = new Item("Item one", true);
            Item two = new Item("Item two", false);

            Regex expOneAsXml = new Regex($@"<Content>Item one</Content>\s*?<IsHeader>true</IsHeader>");
            Regex expTwoAsXml = new Regex($@"<Content>Item two</Content>\s*?<IsHeader>false</IsHeader>");

            List<Item> items = new List<Item> { one, two };


            //**  exercising the code  **//
            string actual = Board.SerializeItems(items);


            //**  testing  **//
            StringAssert.Matches(actual, expOneAsXml);
            StringAssert.Matches(actual, expTwoAsXml);
        }

        [TestMethod()]
        public void SerializeItems__No_Items__Assert_Correct_Xml()  /* working */  {
            //**  groundwork  **//
            List<Item> noItems = new List<Item>();

            // this regex is "all texts starting '<ArrayOfItem', not having a tag close, followed by a self-closing tag"; 
            // this is found with an empty Item[], since if any Items, there would be at least one non- self-closing tag 
            Regex expNotPresent = new Regex(@"<ArrayOfItem[^>]*/>");


            //**  exercising the code  **//
            string actual = Board.SerializeItems(noItems);


            //**  testing  **//
            StringAssert.Matches(actual, expNotPresent);
        }

        #endregion SerializeItems()


        #region ItemsToClipboard()

        [TestMethod()]
        public void ItemsToClipboard__No_Items__Assert_Clipboard_Text_Unchanged()  /* working */  {
            /* if this test fails, a message box appears onscreen; 
             * unfortunately, I can't catch the Exception in my method 
             * _and_ use it to coerce a fail here */

            //**  groundwork  **//
            string expected = "unchanged";
            Clipboard.SetText(expected);
            Thread.Sleep(1);

            List<Item> noItems = new List<Item>();


            //**  exercising the code  **//
            Board.ItemsToClipboard(noItems);


            //**  getting the results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetText();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ItemsToClipboard__Known_Items__Assert_Merged_Text_On_Clipboard()  /* working */  {
            //**  groundwork  **//
            Clipboard.SetText("test failed");
            Thread.Sleep(1);

            Item one = new Item("Item one", true);
            Item two = new Item("Item two", false);

            List<Item> items = new List<Item> { one, two };

            string expected = $"{ one.Content }\r\n{ two.Content }";


            //**  exercising the code  **//
            Board.ItemsToClipboard(items);


            //**  getting the results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetText();


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ItemsToClipboard__Known_Items__Assert_Serial_Data_On_Clipboard()  /* working */  {
            //**  groundwork  **//
            Clipboard.SetText("test failed");
            Thread.Sleep(1);

            Item one = new Item("Item one", true);
            Item two = new Item("Item two", false);

            List<Item> items = new List<Item> { one, two };


            //**  exercising the code  **//
            Board.ItemsToClipboard(items);


            //**  getting the results  **//
            Thread.Sleep(1);
            string actual = Clipboard.GetData(ClipboardFormats.QuickBoardItemArray) as string;


            //**  testing  **//
            Assert.AreNotEqual(null, actual);
            Assert.AreNotEqual(string.Empty, actual);
        }

        [TestMethod()]
        public void ItemsToClipboard__Known_Items__Assert_Correct_Values_In_Custom_Data_Xml()  /* working */  {
            //**  groundwork  **//
            string expected = "unchanged";
            Clipboard.SetText(expected);
            Thread.Sleep(1);

            Item one = new Item("Item one", true);
            Item two = new Item("Item two", false);

            Regex expOneAsXml = new Regex($@"<Content>Item one</Content>\s*?<IsHeader>true</IsHeader>");
            Regex expTwoAsXml = new Regex($@"<Content>Item two</Content>\s*?<IsHeader>false</IsHeader>");

            List<Item> items = new List<Item> { one, two };


            //**  exercising the code  **//
            Board.ItemsToClipboard(items);


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = (string)Clipboard.GetData(ClipboardFormats.QuickBoardItemArray);


            //**  testing  **//
            StringAssert.Matches(actual, expOneAsXml);
            StringAssert.Matches(actual, expTwoAsXml);
        }

        [TestMethod()]
        public void ItemsToClipboard__No_Items__Assert_No_Custom_Data_Xml()  /* working */  {
            //**  groundwork  **//
            string expected = "unchanged";
            Clipboard.SetText(expected);
            Thread.Sleep(1);

            List<Item> noItems = new List<Item>();

            // this regex is "all texts starting '<ArrayOfItem', not having a tag close, followed by a self-closing tag"; 
            // this is found with an empty Item[], since if any Items, there would be at least one non- self-closing tag 
            Regex expNotPresent = new Regex(@"<ArrayOfItem[^>]*/>");


            //**  exercising the code  **//
            Board.ItemsToClipboard(noItems);


            //**  gathering results  **//
            Thread.Sleep(1);
            string actual = (string)Clipboard.GetData(ClipboardFormats.QuickBoardItemArray);


            //**  testing  **//
            Assert.AreEqual(null, actual);
        }

        #endregion ItemsToClipboard()


        #region ItemsFromSerial()

        [TestMethod()]
        public void ItemsFromSerial__Null_Arg__Assert_Returns_Empty_List()  /* working */  {
            //**  groundwork  **//
            string arg = null;

            List<Item> expected = new List<Item>();


            //**  exercising the code  **//
            List<Item> actual = Board.ItemsFromSerial(arg);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ItemsFromSerial__Not_Xml__Assert_Returns_Empty_List()  /* working */  {
            //**  groundwork  **//
            string arg = "not xml";

            List<Item> expected = new List<Item>();


            //**  exercising the code  **//
            List<Item> actual = Board.ItemsFromSerial(arg);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ItemsFromSerial__Invalid_Xml__Assert_Returns_Empty_List()  /* working */  {
            //**  groundwork  **//
            // generating XML that is valid, but not for an Item[] 
            List<int> toSerialize = new List<int> { 3, 4, 5, 6, 7 };
            StringBuilder asBuilder = new StringBuilder();

            using (StringWriter writer = new StringWriter(asBuilder)) {
                XmlSerializer serializer = new XmlSerializer(typeof(List<int>));
                serializer.Serialize(writer, toSerialize);
            }

            string arg = asBuilder.ToString();

            List<Item> expected = new List<Item>();


            //**  exercising the code  **//
            List<Item> actual = Board.ItemsFromSerial(arg);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ItemsFromSerial__Valid_Xml__Assert_Collected_Correct_Items()  /* working */  {
            //**  groundwork  **//
            // generating XML that is valid for an Item[] 
            Item expAt0 = new Item("one", true);  // ensuring .IsHeader is deserialized 
            Item expAt1 = new Item("two");

            Item[] items = { expAt0, expAt1 };  // serialized as array; serializing as List also works (!) 

            StringBuilder asBuilder = new StringBuilder();

            using (StringWriter writer = new StringWriter(asBuilder)) {
                XmlSerializer serializer = new XmlSerializer(typeof(Item[]));
                serializer.Serialize(writer, items);
            }

            string arg = asBuilder.ToString();

            // expecteds 
            List<Item> expecteds = items.ToList();


            //**  exercising the code  **//
            List<Item> actuals = Board.ItemsFromSerial(arg);


            //**  testing  **//
            // testing whole deserialized collection 
            Assert.AreEqual(expecteds.Count, actuals.Count);

            // testing properties of first deserialized Item 
            Item acAt0 = actuals[0];

            Assert.AreNotEqual(null, acAt0);
            Assert.AreEqual(expAt0.Content, acAt0.Content);
            Assert.AreEqual(expAt0.IsHeader, acAt0.IsHeader);

            // testing properties of second deserialized Item 
            Item acAt1 = actuals[1];

            Assert.AreNotEqual(null, acAt1);
            Assert.AreEqual(expAt1.Content, acAt1.Content);
            Assert.AreEqual(expAt1.IsHeader, acAt1.IsHeader);
        }

        #endregion ItemsFromSerial()


        #region ItemsFromClipboard()

        [TestMethod()]
        public void ItemsFromClipboard__Only_Text_On_ClipBoard__Assert_Returns_Empty_List()  /* working */  {
            //**  groundwork  **//
            Clipboard.SetText("unchanged");
            Thread.Sleep(1);

            List<Item> expected = new List<Item>();


            //**  exercising the code  **//
            List<Item> actual = Board.ItemsFromClipboard();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ItemsFromClipboard__Only_Item_On_Clipboard__Assert_Returns_Empty_List()  /* working */  {
            //**  groundwork  **//
            Item item = new Item("unchanged");
            item.ToClipboard();
            Thread.Sleep(1);

            List<Item> expected = new List<Item>();


            //**  exercising the code  **//
            List<Item> actual = Board.ItemsFromClipboard();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void ItemsFromClipboard__Empty_Array_Xml__Assert_Returns_Empty_List()  /* working */  {
            //**  groundwork  **//
            // serializing to clipboard 
            Item[] items = new Item[0];
            StringBuilder asBuilder = new StringBuilder();

            using (StringWriter writer = new StringWriter(asBuilder)) {
                XmlSerializer serializer = new XmlSerializer(typeof(Item[]));
                serializer.Serialize(writer, items);
            }

            string arg = asBuilder.ToString();
            Clipboard.SetData(ClipboardFormats.QuickBoardItemArray, arg);
            Thread.Sleep(1);

            // expecteds, empty 
            List<Item> expecteds = items.ToList();


            //**  exercising the code  **//
            List<Item> actuals = Board.ItemsFromClipboard();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            Assert.AreEqual(expecteds.Count, actuals.Count);
        }

        [TestMethod()]
        public void ItemsFromClipboard__Known_Xml__Assert_Collected_Correct_Items()  /* working */  {
            //**  groundwork  **//
            // initing Item[] 
            Item expAt0 = new Item("one", true);  // ensuring .IsHeader is deserialized 
            Item expAt1 = new Item("two");

            Item[] items = { expAt0, expAt1 };  // serialized as array 

            // serializing to clipboard 
            StringBuilder asBuilder = new StringBuilder();

            using (StringWriter writer = new StringWriter(asBuilder)) {
                XmlSerializer serializer = new XmlSerializer(typeof(Item[]));
                serializer.Serialize(writer, items);
            }

            string arg = asBuilder.ToString();
            Clipboard.SetData(ClipboardFormats.QuickBoardItemArray, arg);
            Thread.Sleep(1);

            // expecteds 
            List<Item> expecteds = items.ToList();


            //**  exercising the code  **//
            List<Item> actuals = Board.ItemsFromClipboard();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            // testing whole deserialized collection 
            Assert.AreEqual(expecteds.Count, actuals.Count);

            // testing properties of first deserialized Item 
            Item acAt0 = actuals[0];

            Assert.AreNotEqual(null, acAt0);
            Assert.AreEqual(expAt0.Content, acAt0.Content);
            Assert.AreEqual(expAt0.IsHeader, acAt0.IsHeader);

            // testing properties of second deserialized Item 
            Item acAt1 = actuals[1];

            Assert.AreNotEqual(null, acAt1);
            Assert.AreEqual(expAt1.Content, acAt1.Content);
            Assert.AreEqual(expAt1.IsHeader, acAt1.IsHeader);
        }

        [TestMethod()]
        public void ItemsFromClipboard__Round_Trip__Assert_Correct_Collected_Items()  /* working */  {
            //**  groundwork  **//
            Item expAt0 = new Item("one", true);  // ensuring .IsHeader is deserialized 
            Item expAt1 = new Item("two");

            List<Item> items = new List<Item> { expAt0, expAt1 };
            Board.ItemsToClipboard(items);
            Thread.Sleep(1);

            List<Item> expecteds = items.ToList();


            //**  exercising the code  **//
            List<Item> actuals = Board.ItemsFromClipboard();


            //**  cleaning up  **//
            Thread.Sleep(1);


            //**  testing  **//
            // testing whole deserialized collection 
            Assert.AreEqual(expecteds.Count, actuals.Count);

            // testing properties of first deserialized Item 
            Item acAt0 = actuals[0];

            Assert.AreNotEqual(null, acAt0);
            Assert.AreEqual(expAt0.Content, acAt0.Content);
            Assert.AreEqual(expAt0.IsHeader, acAt0.IsHeader);

            // testing properties of second deserialized Item 
            Item acAt1 = actuals[1];

            Assert.AreNotEqual(null, acAt1);
            Assert.AreEqual(expAt1.Content, acAt1.Content);
            Assert.AreEqual(expAt1.IsHeader, acAt1.IsHeader);
        }

        #endregion ItemsFromClipboard()


        #region InsertItems()

        [TestMethod()]
        public void InsertItems__No_Items__Assert_No_Change_To_Board()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoard();

            List<Item> empty = new List<Item>();
            int at = board.Count / 3;

            List<Item> expected = board.ToList();


            //**  exercising the code  **//
            board.InsertItems(empty, at);


            //**  gathering results  **//
            List<Item> actual = board.ToList();


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertItems__At_0__Assert_Correct_Board_Contents()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoard();

            List<Item> items = new List<Item> { new Item("new 1"), new Item("new 2") };
            int at = 0;

            List<Item> expected = board.ToList();
            expected.Insert(0, items[0]);
            expected.Insert(1, items[1]);


            //**  exercising the code  **//
            board.InsertItems(items, at);


            //**  gathering results  **//
            List<Item> actual = board.ToList();


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertItems__At_End__Assert_Correct_Board_Contents()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoard();

            List<Item> items = new List<Item> { new Item("new 1"), new Item("new 2") };
            int at = board.Count;  // end of collection; in contrast, ×[.Count -1] inserts _before_ last Item 

            // adding is equivalent to inserting at end 
            List<Item> expected = board.ToList();
            expected.Add(items[0]);
            expected.Add(items[1]);


            //**  exercising the code  **//
            board.InsertItems(items, at);


            //**  gathering results  **//
            List<Item> actual = board.ToList();


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertItems__At_Middle_Index__Assert_Correct_Board_Contents()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoard();

            List<Item> items = new List<Item> { new Item("new 1"), new Item("new 2") };
            int at = board.Count / 2;  // at or close to the middle 

            List<Item> expected = board.ToList();
            expected.Insert(at, items[0]);
            expected.Insert(at + 1, items[1]);


            //**  exercising the code  **//
            board.InsertItems(items, at);


            //**  gathering results  **//
            List<Item> actual = board.ToList();


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        #endregion InsertItems()


        #region InsertItemsFromClipboard()

        [TestMethod()]
        public void InsertItemsFromClipboard__Only_Text_On_ClipBoard__Assert_Board_Unchanged()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoard();
            Clipboard.SetText("text");
            Thread.Sleep(1);

            List<Item> expected = board.ToList();


            //**  exercising the code  **//
            board.InsertItemsFromClipboard(board.Count - 1);


            //**  gathering results  **//
            List<Item> actual = board.ToList();


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertItemsFromClipboard__Empty_Array_On_Clipboard__Assert_Board_Unchanged()  /* working */  {
            //**  groundwork  **//
            SetClipboardToEmptyArrayAndText();
            Thread.Sleep(1);

            Board board = FiveItemBoard();

            List<Item> expected = board.ToList();


            //**  exercising the code  **//
            board.InsertItemsFromClipboard(board.Count / 3);


            //**  gathering results  **//
            List<Item> actual = board.ToList();


            //**  testing  **//
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void InsertItemsFromClipboard__At_Middle_Index__Assert_Correct_Board_Contents()  /* working */  {
            //**  groundwork  **//
            List<Item> items = new List<Item> { new Item("new 1", true), new Item("new 2") };
            Board.ItemsToClipboard(items);
            Thread.Sleep(1);

            Board board = FiveItemBoard();
            int at = board.Count / 3;  // somewhere in the middle 

            // expecteds 
            List<Item> expecteds = board.ToList();
            expecteds.Insert(at, items[0]);
            expecteds.Insert(at + 1, items[1]);


            //**  exercising the code  **//
            board.InsertItemsFromClipboard(at);


            //**  gathering results  **//
            List<Item> actuals = board.ToList();


            //**  testing  **//
            // can't use CollectionAssert because Items deserialized 
            // from clipboard are identical, but different instances 
            for (int i = 0; i < expecteds.Count; i++) {
                Item expected = expecteds[i];
                Item actual = actuals[i];

                Assert.AreEqual(expected.Content, actual.Content);
                Assert.AreEqual(expected.IsHeader, actual.IsHeader);
            }
        }

        #endregion InsertItemsFromClipboard()

    }
}
