﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Reflection;
using QuickBoard;

namespace QuickBoardTests
{
    [TestClass]
    public class MainWindowTests
    {
        #region Fixtures

        private static Board FiveItemBoard()  /* verified */  {
            Board board = new Board();

            board.Add(new Item("first"));
            board.Add(new Item("second"));
            board.Add(new Item("third"));
            board.Add(new Item("fourth"));
            board.Add(new Item("fifth"));

            return (board);
        }

        #endregion Fixtures


        #region SupplyCurrentOffset()

        [TestMethod()]
        public void SupplyCurrentOffset__Null_Parameter__Assert_Returns_Count_Of_Items()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoard();
            MainWindow window = new MainWindow();
            PrivateObject p = new PrivateObject(window);

            p.SetField("_board", board);

            object arg = null;

            int expected = board.Count;


            //**  exercising the code  **//
            int actual = (int)p.Invoke("SupplyCurrentOffset", arg);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SupplyCurrentOffset__Non_Item_Parameter__Assert_Returns_Count_Of_Items()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoard();
            MainWindow window = new MainWindow();
            PrivateObject p = new PrivateObject(window);

            p.SetField("_board", board);

            // a realistic type for UI context 
            object arg = new Button();

            int expected = board.Count;


            //**  exercising the code  **//
            int actual = (int)p.Invoke("SupplyCurrentOffset", arg);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SupplyCurrentOffset__Empty_Board__Assert_Returns_Zero()  /* working */  {
            //**  groundwork  **//
            MainWindow window = new MainWindow();
            PrivateObject p = new PrivateObject(window);

            p.SetField("_board", new Board());

            // a realistic type for UI context 
            object arg = new Button();

            int expected = 0;


            //**  exercising the code  **//
            int actual = (int)p.Invoke("SupplyCurrentOffset", arg);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SupplyCurrentOffset__Item_At_0__Assert_Returns_Zero()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoard();
            MainWindow window = new MainWindow();
            PrivateObject p = new PrivateObject(window);

            p.SetField("_board", board);

            Item item = board[0];
            object arg = item;

            int expected = board.IndexOf(item);


            //**  exercising the code  **//
            int actual = (int)p.Invoke("SupplyCurrentOffset", arg);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SupplyCurrentOffset__Item_At_End__Assert_Returns_Top_Offset()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoard();
            MainWindow window = new MainWindow();
            PrivateObject p = new PrivateObject(window);

            p.SetField("_board", board);

            Item item = board[board.Count - 1];  // top offset on Board 
            object arg = item;

            int expected = board.IndexOf(item);


            //**  exercising the code  **//
            int actual = (int)p.Invoke("SupplyCurrentOffset", arg);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void SupplyCurrentOffset__Middle_Item_Parameter__Assert_Returns_Item_Offset()  /* working */  {
            //**  groundwork  **//
            Board board = FiveItemBoard();
            MainWindow window = new MainWindow();
            PrivateObject p = new PrivateObject(window);

            p.SetField("_board", board);

            Item item = board[2];
            object arg = item;

            int expected = board.IndexOf(item);


            //**  exercising the code  **//
            int actual = (int)p.Invoke("SupplyCurrentOffset", arg);


            //**  testing  **//
            Assert.AreEqual(expected, actual);
        }

        #endregion SupplyCurrentOffset()

    }
}
